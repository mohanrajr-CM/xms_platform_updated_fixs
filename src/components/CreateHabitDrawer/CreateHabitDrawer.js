import React from 'react'
import './CreateHabitDrawer.scss';
import Drawer from '@material-ui/core/Drawer';
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ListItemText from '@material-ui/core/ListItemText';
import Input from '@material-ui/core/Input';
import Checkbox from '@material-ui/core/Checkbox';
import DatePicker from 'react-date-picker';
import moment from 'moment';
import Button from '@material-ui/core/Button';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { TwitterPicker } from 'react-color'
import { Scrollbars } from 'react-custom-scrollbars';

import colorPallet from '../../assets/create-habit/Icon material-color-lens.svg'
import locationIcon from '../../assets/create-habit/Icon material-my-location.svg'
import tagsIcon from '../../assets/create-habit/Icon awesome-tags.svg'
import startIcon from '../../assets/create-habit/Icon material-star.svg'
import profileIcon from '../../assets/create-habit/Icon awesome-user-alt.svg'
import incrementorIcon from '../../assets/create-habit/Icon feather-plus.svg'
import decrementorIcon from '../../assets/create-habit/Icon feather-minus.svg'
import closeCreateHabitIcon from '../../assets/create-habit/Icon ionic-ios-close.svg'
import emailIcon from '../../assets/icons/01-10-2019/Icon feather-mail.svg'

import hTemplateBooks from '../../assets/icons/LoginAndRegistration_icons/books.png'
import hTemplateWashFace from '../../assets/icons/LoginAndRegistration_icons/wash-face.png'
import hTemplateSleep from '../../assets/icons/LoginAndRegistration_icons/sleep.png'
import hTemplateTimeManagement from '../../assets/icons/LoginAndRegistration_icons/time-management.png'

import CompanyLogo from "../../assets/icons/01-10-2019/company-logo.svg"
import CompanyEditIcon from "../../assets/icons/SVG/Iconfeather-edit-3.svg";
import EditCompanyDetails from '../../components/TicketingSystem/CreateTicket/EditCompanyDetails/EditCompanyDetails';

import DropDwnIcon from "../../assets/icons/SVG/Icon ionic-ios-arrow-down.svg";
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import QckCreateChck from "../../assets/icons/01-10-2019/Icon feather-check-circle.svg";
import profile from "../../assets/images/profile.png";
import AddMultipleTag from "../../assets/icons/01-10-2019/Icon feather-plus-circle.svg";

import DrpDwnIcn from "../../assets/icons/create-ticket/Icon ionic-md-arrow-dropdown.svg";
import axios from "axios";
import { baseUrl } from "../../constants";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Radio from '@material-ui/core/Radio';

const teamTabPanel = []

const peopleTabPanel = []

const QuickCreateIcons = []

const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder',
];
const buildQuitBtns = [
  { btnId: 1, btnName: 'BUILD', },
  { btnId: 2, btnName: 'QUIT', }
]
const goalPeriodBtns = [
  { btnId: 1, btnName: 'DAILY', },
  { btnId: 2, btnName: 'WEEKLY', },
  { btnId: 3, btnName: 'MONTHLY', },
  { btnId: 4, btnName: 'YEARLY', },
]
const weekDaysBtns = [
  { btnId: 0, isActive: false, btnName: 'SUN', },
  { btnId: 1, isActive: false, btnName: 'MON', },
  { btnId: 2, isActive: false, btnName: 'TUE', },
  { btnId: 3, isActive: false, btnName: 'WED', },
  { btnId: 4, isActive: false, btnName: 'THU', },
  { btnId: 5, isActive: false, btnName: 'FRI', },
  { btnId: 6, isActive: false, btnName: 'SAT', },
]
const habitTemplateData = [
  { templateName: 'Must Have Habits', templateDscr: 'Small habits, big results', tmplImage: hTemplateBooks },
]
const colors = ['#1abc9c', '#17a085', '#2ecc71', '#27ae60', '#3498db', '#2980b9', '#9b59b6', '#8e44ad', '#34495e', '#2c3e50', '#f1c40e', '#f39c12', '#d35400', '#e74c3c', '#c0392b', '#9b0000', '#f28a8a', '#00edff', '#1aa0bc', '#1cd8ff', '#ff92f4', '#d500a3', '#ffb300', '#d0cfec', '#ecf1f9', '#c8c8c8', '#656565', '#464646']

let wrapperRef;
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

class CreateHabitDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.colorPallet = null;
    this.state = {
      personName: [],
      nmbTimesPerDay: 1,
      buildQuitBtnId: 1,
      goalPeriodBtnId: 4,
      weekDaysBtnId: 3,
      selectedDate: '',
      isShareThroughEmail: false,
      isShowColorPallet: false,
      habitTitleColor: '',
      isShowSetTimePopup: false,
      setTimeBtnId: null,
      setTimeBtns: [],
      weekDaysBtns: weekDaysBtns,
      habitTitle: "",
      count: 1,
      setTime: [],
      location: "",
      tags: [],
      tags1: [],
      users: [],
      users1: [],
      selectedResponsible:[],
      shareMail: "",
      saveTemplate: false,
      frequency: {},
      responsibleUsers: [],
      getAllHabitTemplates: [],
      isEditCompDtlsOpen: false,

      isShowDropdown: false,
      isMultiTagDropdown: false,
      selectedItems: [],
      selectedPeople: [],
      selectedTeams: [],
      valueTab: 0,
      SelectedValue: 'a',
      assignedToPeople: null,
      assignedToTeam: null,
      teams: [],
      isEditCompDtlsOpen: false,
      userData:[],  selectedCompany:{},
      departments:[],
      selectedDepartment:{},
      teamsData:[],
      selectedTeam:{},
      logedUser:{},

    }
  }
  componentDidMount() {
    this.addNewTimerBtn()
    document.addEventListener('mousedown', this.handleClickOutside);
    this.fetchUsers();
    let requestBody3 = {
      query: `
        query getUserById($id:Int!) 
          {
            getUserById(id:$id){
              id
              username,
              emailIs,
              firstName,
              lastName,
              tenantId,
              departmentId,
              companyId,
            }
          }
          `,
          variables: {
            id:parseInt(localStorage.getItem('id')),
          }
    };

      axios({
        method: 'post',
        url: baseUrl.server,
        data: requestBody3,
        headers: {
            'Content-type': 'application/json'
        }
    }).then(res => {
      this.setState({logedUser:res.data.data.getUserById})
      this.fetchTeams(res.data.data.getUserById.companyId,res.data.data.getUserById.departmentId,res.data.data.getUserById.tenantId);
      this.fetchcompany(res.data.data.getUserById.companyId);
      this.fetchDepartment(res.data.data.getUserById.departmentId);
      this.fetchTags(res.data.data.getUserById.companyId);
        return res;
    }).catch(err => {
      console.log("Error in user==",err)
        return err;
    });
    let requestBody1 = {
      query: `
        query allTags($companyId:Int!) 
          {
            allTags(companyId:$companyId){
              id,
               tagTitle
              }
          }
      `,
      variables: {
        companyId: 1,
      }
    };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody1,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(res => {
      // console.log("tags==",res.data.data.allTags)
      this.setState({ tags: res.data.data.allTags })
      return res
    }).catch(err => {
      // console.log("Tags==",err)
      return err;
    });

    let requestBody2 = {
      query: `
    query getAllUsers 
      {
        getAllUsers{
          id,
           firstName,
           lastName,
           emailIs
          }
      }
  `,
      // variables: {
      //   companyId: 1,
      // }
    };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody2,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(res => {
      this.setState({ users: res.data.data.getAllUsers })
      return res
    }).catch(err => {
      return err;
    });
  }
  fetchcompany = (companyId) => {
    const requestBody = {
        query: `
          query {
            getAllCompany {
              id,
              companyName,
              companyDescription
            }
          }
        `
    };
  
    fetch(baseUrl.server, {
        method: 'POST',
        body: JSON.stringify(requestBody),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            if (res.status !== 200 && res.status !== 201) {
                throw new Error('Failed!');
            }
            return res.json();
        })
        .then(resData => {
            let company = resData.data.getAllCompany;
            if(company!=null || company != undefined){
            company.map(cmp=>{
              if(companyId == cmp.id){
                this.setState({selectedCompany:cmp})
              }
            })
          }
            this.setState({ company: company });
        })
  
        .catch(err => {
            console.log(err);
        });
  };


  fetchDepartment = (deptId) => {
    const requestBody1 = {
        query: `
          query {
            getAllDepartments {
              id,
              departmentName
            }
          }
        `
    };
  
    fetch(baseUrl.server, {
        method: 'POST',
        body: JSON.stringify(requestBody1),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            if (res.status !== 200 && res.status !== 201) {
                throw new Error('Failed!');
            }
            return res.json();
        })
        .then(resData => {           
            let departments = resData.data.getAllDepartments;
            if(departments !=null || departments != undefined){
            departments.map(depart=>{
              if(deptId == depart.id){
                this.setState({selectedDepartment:depart})
              }
            }) 
          }
            this.setState({ departments: departments });
        })
  
        .catch(err => {
            console.log(err);
        });
  };

fetchTags=(companyId)=>{
  let requestBody2 = {
    query: `
  query allTags($companyId:Int!) 
    {
      allTags(companyId:$companyId){
        id,
         tagTitle
        }
    }
`,
    variables: {
      companyId: companyId,
    }
  };

  axios({
    method: 'post',
    url: baseUrl.server,
    data: requestBody2,
    headers: {
      'Content-type': 'application/json'
    }
  }).then(res => {
    this.setState({ tags: res.data.data.allTags })
    return res
  }).catch(err => {
    console.log("ErrorTags==",err)
    return err;
  });
}

  fetchTeams = (a,b,c) => {
    const requestBody = {
        query: `
          query getTeamByCompanyIdAndDepartmentId($companyId:Int,$departmentId:Int,$tenantId:Int)
             {
              getTeamByCompanyIdAndDepartmentId(companyId:$companyId,departmentId:$departmentId,tenantId:$tenantId){
              id,
              teamName
              }
            }
          `, variables:{
          companyId:a,
          departmentId : b,
          tenantId : c
        }
    };
  
    axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody,
      headers: {
          'Content-type': 'application/json'
      }
  }).then(resData => {
            let teamsData = resData.data.data.getTeamByCompanyIdAndDepartmentId;
            this.setState({ teamsData: teamsData, selectedTeam:teamsData[0] });
        })
        .catch(err => {
            console.log(err);
        });
  };
  fetchUsers = () => {
    const requestBody = {
        query: `
          query {
            getAllUsers {
              id,
              firstName,
              lastName,
              emailIs,
              password,
              userRollId,
              companyId,
              departmentId
            }
          }
        `
    };
  
    fetch(baseUrl.server, {
        method: 'POST',
        body: JSON.stringify(requestBody),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            if (res.status !== 200 && res.status !== 201) {
                throw new Error('Failed!');
            }
            return res.json();
        })
        .then(resData => {
            //console.log(resData);
            let userData = resData.data.getAllUsers;
            this.setState({ userData: userData });
        })
        .catch(err => {
            console.log(err);
        });
  };
  openEditCompDtlsOpen=(e)=>{
    e.stopPropagation()
     this.setState({ isEditCompDtlsOpen: true })
    }

  // componentWillMount() {
  //   document.removeEventListener('mousedown', this.handleClickOutside);
  // }
  handleClickOutside = (event) => {
    if (wrapperRef && !wrapperRef.contains(event.target)) {
      this.setState({ isShowColorPallet: false, isShowSetTimePopup: false })
    }
  }
  closeCompanyDrawer=()=>{
    this.setState({isEditCompDtlsOpen: false})
  }
  setWrapperRef = (node) => wrapperRef = node;
  button = (btnName, style, onClick) => (
    <Button variant="contained" className={style} onClick={onClick}>
      {btnName}
    </Button>
  )
  addNewTimerBtn = () => {
    let length = this.state.setTimeBtns.length;
    let timeObj = { id: length, setTime: '12:00 AM', time: ['12:00 AM', '1:00 AM', '2:00 AM', '3:00 AM', '4:00 AM', '5:00 AM', '6:00 AM', '7:00 AM', '8:00 AM', '9:00 AM', '10:00 AM', '11:00 AM', '12:00 PM', '1:00 PM', '2:00 PM', '3:00 PM', '4:00 PM', '5:00 PM', '6:00 PM', '7:00 PM', '8:00 PM', '9:00 PM', '10:00 PM', '11:00 PM'] };
    if (this.state.setTimeBtns.length < 4) this.setState({ setTimeBtns: [...this.state.setTimeBtns, timeObj] })
  }
  removeOneTimerBtn = (id) => {
    let setTimeBtns = this.state.setTimeBtns;
    setTimeBtns.splice(id, 1);
    this.setState({ setTimeBtns: setTimeBtns })
  }

  teamHandleClickfunction = (e, index, name) => {
    this.setState({ assignedToTeam: index });
    if (this.state.selectedTeams.includes(name)) { } else {
      this.state.selectedTeams.push(index)
      this.state.selectedItems.push(name)
    }
  }

  peopleHandleClickfunction1 = (e, index, name) => {
    this.setState({ [e.target.name]: index });
    if (this.state.selectedPeople.includes(name)) { } else {
      this.state.selectedPeople.push(name)
      this.state.selectedItems.push(name)
    }

  }

  radioHandleChange = event => {
    this.setState(event.target.value);
  };

  tabHandleChange = (event, newValue) => {
    this.setState({ valueTab: newValue });
  };

  doChange = () => {
    this.setState({
      text: 'After change',

    })
  }
  showDropdown = () => {
    this.setState({ isShowDropdown: !this.state.isShowDropdown })
  }

  showMultiTagDropdown = () => {
    this.setState({ isMultiTagDropdown: !this.state.isMultiTagDropdown })
  }

  SelectMenuChangeHandler = event => {
    console.log("SelectedHere==Name==", event.target.name)
    console.log("SelectedHere==Value", event.target.value)
    this.setState({ [event.target.name]: event.target.value })
  }

  SelectMenuChangeResponsible = event => {
    let data = event.target.value;
    this.state.users.map(data1=>{
      if(data==data1.firstName){
        this.state.responsibleUsers.push(data1.id)
      }
    })
    this.setState({ [event.target.name]: event.target.value })
  }
  checkBoxHandler = event => this.setState({ isShareThroughEmail: event.target.checked });
  checkBoxHandlerTemplate = event => {
    this.setState({ saveTemplate: event.target.checked });
  }
  closeDrawerHandler = () => this.props.thisObj.setState({ isOpenCreateHabit: false });
  buildQuitBtnClickHandler = button => this.setState({ buildQuitBtnId: button.btnId });
  goalPeriodBtnClickHandler = button => this.setState({ goalPeriodBtnId: button.btnId });
  handleDateChange = date => this.setState({ selectedDate: date });
  showColorPalletPopup = () => this.setState({ isShowColorPallet: !this.state.isShowColorPallet });
  showSetTimePopup = (id) => this.setState({ isShowSetTimePopup: !this.state.isShowSetTimePopup, setTimeBtnId: id });
  handleChangeComplete = (color, event) => this.setState({ habitTitleColor: color.hex });
  weekDaysBtnClickHandler = button => {
    let tempWeekDaysBtns = this.state.weekDaysBtns;
    console.log('button_button123', button, "btn 123==", tempWeekDaysBtns)
    tempWeekDaysBtns[button.btnId].isActive = !tempWeekDaysBtns[button.btnId].isActive
    this.setState({ weekDaysBtnId: button.btnId, frequency: tempWeekDaysBtns });
  }

  incrementHandler = () => {
    if (this.state.nmbTimesPerDay < 4) {
      this.setState({ nmbTimesPerDay: this.state.nmbTimesPerDay + 1 })
      this.addNewTimerBtn()
    }
  }
  decrementHandler = () => {
    if (this.state.nmbTimesPerDay > 1) {
      this.setState({ nmbTimesPerDay: this.state.nmbTimesPerDay - 1 })
      this.removeOneTimerBtn(this.state.nmbTimesPerDay - 1)
    }

  }

  colorPalletPopup = () => (
    <div className='color-pallet-popup' ref={this.setWrapperRef}>
      <span>Color palette</span>
      <TwitterPicker colors={colors} onChangeComplete={this.handleChangeComplete} />
    </div>
  )
array=[]
  setTimeHandler = (time, timeToSet) => {
    this.state.setTime[time.id]= timeToSet
    let temp = this.state.setTimeBtns;
    temp[time.id].setTime = timeToSet
  }
  setTimePopup = (time) => (
    <div className='set-time-popup' ref={this.setWrapperRef}>
      {console.log('time_time', time)}
      <Scrollbars className='time-scroll-container'>
        {time ? time.time.map(indTime => <li onClick={() => this.setTimeHandler(time, indTime)}>{indTime}</li>) : ''}
      </Scrollbars>
    </div>
  )

  changeSetTime = (e) => {
    let setTime1 = this.state.setTime;
    setTime1.push(e.target.value);
    console.log("hey there", setTime1);
  }
  changeLocation = (e) => {
    e.preventDefault();
    this.setState({ location: e.target.value });
  }
  fetchUsers = () => {
    const requestBody = {
      query: `
          query {
            getAllUsers {
              id,
              firstName,
              lastName,
              emailIs,
              password,
              userRollId,
              companyId,
            }
          }
        `
    };

    fetch(baseUrl.server, {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error('Failed!');
        }
        return res.json();
      })
      .then(resData => {
        let userData = resData.data.getAllUsers;
        this.setState({ userData: userData });
      })
      .catch(err => {
        console.log(err);
      });
  };
  submitHandler = async (e) => {
    // console.log("OnSubmit=",)
    e.preventDefault();

    let requestBody = {
      query: `
          mutation addHabit(
              $habitTitle: String, $count: Int!,$setTime:[String],$assignBy:Int,
              $tenantId:Int,$location:String,$goal:Int,$shareMail:String,
              $isShareThroughEmail:Boolean,
              $saveTemplate:Boolean,$frequency:JSON,
              $responsibleUsers:[Int],
              $tags1:[String],
            ) 
            {
              addHabit(
                habitTitle: $habitTitle, count: $count,
                setTime:$setTime,tenantId:$tenantId,
                assignBy:$assignBy,
                location:$location,
                goal:$goal,
                shareMail:$shareMail,
                isShareThroughEmail:$isShareThroughEmail,
                saveTemplate:$saveTemplate,
                frequency:$frequency,responsibleUsers:$responsibleUsers,
                tags1:$tags1
                )
                {
                  id,
                  habitTitle,
                  habitCount,
                  progressPercent,
                }
            }
        `,
      variables: {
        habitTitle: this.state.habitTitle,
        tenantId: 1,
        count: this.state.nmbTimesPerDay,
        setTime: this.state.setTime,
        location: this.state.location,
        assignBy: 1,
        goal: this.state.goalPeriodBtnId,
        shareMail: this.state.shareMail,
        isShareThroughEmail: this.state.isShareThroughEmail,
        saveTemplate: this.state.saveTemplate,
        frequency: this.state.frequency,
        responsibleUsers: this.state.responsibleUsers,
        tags1: this.state.tags1
        // user_id: this.state.user_id,
        // team_id: Number(this.state.team_id),
        // tags: this.state.tags,
        // ticket_supportive: this.state.attachment,
        // notify_others: notify_others
      }
    };

    let resData = await axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody,
      headers: {
        'Content-type': 'application/json'
      }
    }).then(res => {
      console.log("hey there", requestBody)
      return res
    }).catch(err => {
      console.log("hey there", requestBody)
      return err;
    });
    this.closeDrawerHandler();

    // if (resData.status == 200) {
    //     window.location.href = '/ticketlisting';
    // } else {
    //     window.location.href = '/createticket';
    // }
  }

  handleChangeShare = (e) => {
    this.setState({ shareMail: e.target.value, isShareThroughEmail: true })
    // console.log("shareMail",e.target.value)
  }
  onChangeHabitTitle = (e) => {
    this.setState({ habitTitle: e.target.value })
  }

  sideList = () => (
    <div>
      {this.state.isEditCompDtlsOpen ? <EditCompanyDetails onClick={(e)=>e.stopPropagation()} isEditCompDtlsOpen={this.state.isEditCompDtlsOpen} allState={this.state} thisObj={this}/>:null}
      <div className='create-habit-layout'>
        <div className="create-habit-close-icon" onClick={this.closeDrawerHandler}>
          <img src={closeCreateHabitIcon} alt='close-icon' />
        </div>

        <div className='create-habit-main-section'>
          <div className="create-habit-header-section">
            <p>Create Habits</p>
            <p>Lorem ipsum is simply dummy text of the printing and <br></br>typesetting industry</p>
          </div>

          <div className='create-habit-title-container'>
            <>
              <img src={colorPallet} alt='colorPallet' className='colorPallet' onClick={this.showColorPalletPopup} />
              {this.state.isShowColorPallet ? this.colorPalletPopup() : null}
              <TextField
                id="standard-basic"
                className={''}
                value={this.state.habitTitle}
                margin="normal"
                placeholder='Habit title *'
                onChange={this.onChangeHabitTitle}
              />
            </>
            <Fab variant="extended" aria-label="delete" className='create-habit-fav-button'>
              <img src={startIcon} alt='startIcon' />
              <input
                    type="file"
                    className="custom-file-input"
                    id="inputGroupFile01"
                  />
              <span>ICON</span>
            </Fab>
          </div>

          <div className='flex-container'>
            <FormControl className='multi-select-dropdown-menu'>
              <InputLabel htmlFor="age-simple"><img src={profileIcon} alt='profileIcon' /><span>Responsible</span> <i class="fa fa-asterisk edtCmp-icon" aria-hidden="true"></i></InputLabel>
              <Select name='users1' multiple value={this.state.users1} onChange={this.SelectMenuChangeResponsible} input={<Input id="select-multiple-checkbox" />} renderValue={selected => selected.join(', ')} MenuProps={'MenuProps'}>
                {
                  this.state.users ? this.state.users.map(name => (
                    <MenuItem key={name.firstName} value={name.firstName}>
                      <Checkbox checked={this.state.users1.indexOf(name.firstName) > -1} />
                      <ListItemText primary={name.firstName} />
                    </MenuItem>
                  )) : ''}
              </Select>
            </FormControl>

            <div className='create-habit-start-date'>
              <DatePicker className={`date-picker-style`} onChange={this.handleDateChange} clearIcon={null} />
              <TextField className={''} value={this.state.selectedDate ? moment(this.state.selectedDate).format('MMM Do, YYYY') : null} label={this.state.selectedDate ? null : 'Start Date'} margin="normal" />
            </div>
          </div>

          <div className='build-quit-section'>
            <span>Build or quit this habit?</span> {buildQuitBtns.map(button => this.button(button.btnName, `custome-button ${button.btnId === this.state.buildQuitBtnId ? 'custome-button-active' : 'custome-button-dashed'}`, () => this.buildQuitBtnClickHandler(button)))}
          </div>

          <div className='build-quit-section'>
            <span>Choose your goal period:</span> {goalPeriodBtns.map(button => this.button(button.btnName, `custome-button ${button.btnId === this.state.goalPeriodBtnId ? 'custome-button-active' : 'custome-button-dashed'}`, () => this.goalPeriodBtnClickHandler(button)))}
          </div>

          <div className='set-your-goal-section'>
            <div className='left-section'>
              <span>Set your goal:</span>
              <div className='left-section-content'>
                <div className='number-times-perday'>
                  <div onClick={this.decrementHandler}><img src={decrementorIcon} alt='decrementorIcon' /></div>
                  <div>{this.state.nmbTimesPerDay}</div>
                  <div onClick={this.incrementHandler}><img src={incrementorIcon} alt='decrementorIcon' /></div>
                </div>
                <span>or more times per day</span>
              </div>
            </div>


            <div className='right-section'>
              <span>Set time:</span>
              <div className='right-section-content' onChange={this.changeSetTime}>
                {
                  this.state.setTimeBtns ?
                    this.state.setTimeBtns.map(time => (
                      <div className='timer-section' onClick={() => this.showSetTimePopup(time.id)}>{time.setTime} {this.state.isShowSetTimePopup && this.state.setTimeBtnId === time.id ? this.setTimePopup(time) : null}</div>
                    )) : ''
                }
              </div>
            </div>
          </div>

          <div className='goals-on-which-days-section'>
            <span>Track goals on which days?</span>
            <div className='days-button-container'>
              {this.state.weekDaysBtns.map(weekBtn => <div className={`day-button ${weekBtn.isActive ? 'active-day-button' : 'non-active-day-button'}`} onClick={() => this.weekDaysBtnClickHandler(weekBtn)}>{weekBtn.btnName}</div>)}
            </div>
          </div>

          <div className='location-tags-notes-section'>
            <div className='left-section'>
              <div>
                <div className='create-habit-location'>
                  <img src={locationIcon} alt='' />
                  <TextField className={''} value={this.state.location} onChange={this.changeLocation} label={'Location'} margin="normal" />
                </div>

                <FormControl className='multi-select-dropdown-menu'>
                  <InputLabel htmlFor="age-simple"><img src={tagsIcon} alt='tagsIcon' /><span>Tags</span> <i class="fa fa-asterisk edtCmp-icon" aria-hidden="true"></i></InputLabel>
                  <Select name='tags1' multiple value={this.state.tags1} onChange={this.SelectMenuChangeHandler} input={<Input id="select-multiple-checkbox" />} renderValue={selected => selected.join(', ')} MenuProps={'MenuProps'}>
                    {this.state.tags ? this.state.tags.map(name => (
                      <MenuItem key={name.id} value={name.tagTitle}>
                        <Checkbox checked={this.state.tags1.indexOf(name.tagTitle) > -1} />
                        <ListItemText primary={name.tagTitle} />
                      </MenuItem>
                    )) : ''}
                  </Select>
                </FormControl>

              </div>
            </div>
            <div className='right-section'>
              <TextareaAutosize className='notes-text-area' placeholder="Notes" />
            </div>
          </div>
        </div>
        <div className="create-habit-body-right">
     
        <div className="create-ticket-drawer-company-details">
          <div className="create-ticket-company-description d-flex justify-space-between">
            <div className="d-flex">
              <img src={CompanyLogo} alt='CompanyLogo' />
              {/* {this.state.company.map(company=>{
                if(this.state.logedUser.companyId == company.id){ */}
                            <div className="create-ticket-drawer-company-text">
                            
                                <p className="create-ticket-drawer-company-text-one">{this.state.selectedCompany.companyName}</p>
                                <p className="create-ticket-drawer-company-text-two">{this.state.selectedCompany.companyDescription}</p>
                            </div>
              {/* }} )} */}
            </div>
            <div className="create-ticket-drawer-company-details-edit">
                <img src={CompanyEditIcon} onClick={(e)=>this.openEditCompDtlsOpen(e)} alt='CompanyEditIcon'/>
            </div>
          </div>
          <div className="department-and-team-name">

            {/* { this.state.departments != null || this.state.departments != undefined ? */}
              {/* this.state.departments.map(dept=>{ */}
                {/* if(this.state.logedUser.departmentId == dept.id) */}
                     { this.state.selectedDepartment ?  <p>{this.state.selectedDepartment.departmentName}</p> : <p>Department Name</p>
                    }
              {/* }) */}
              {/* : null */}
            {/* } */}
            {
              this.state.selectedTeam.teamName ? <p className="team-name">{this.state.selectedTeam.teamName}</p> : <p className="team-name">Team Name</p>
            }
            
          </div>
        </div>

          <div className='create-habit-sub-section'>
            <div className='create-habit-templates-section'>
              <div className='create-habit-templates-heading'>
                <span>Habit ideas</span>
                <span>See all</span>
              </div>

              {
               habitTemplateData ? habitTemplateData.map(template => {
                  return (
                    <div className='habit-templates-card'>
                      <div className='habit-templates-card-details'>
                        <p>{template.templateName}</p>
                        <p>{template.templateDscr}</p>
                      </div>
                      <div className='habit-templates-card-image'>
                        <img src={template.tmplImage} alt='hTemplateBook' />
                      </div>
                    </div>
                  )
                }) :''
              }
            </div>

            <div className='create-habit-attachment'>
              <span>Attachments</span>
              <div>
                <Button className='file-attach-button'>
                  <input
                    type="file"
                    className="custom-file-input"
                    id="inputGroupFile01"
                  />
                </Button>
                <p>Drag attach file, <br />or <span>browse</span></p>
              </div>
            </div>

            <div className='share-through-email-section'>
              <div>
                <span>Share</span>
                <Checkbox className='share-through-email-checkbox' checked={this.state.isShareThroughEmail} onChange={this.checkBoxHandler} value="checkedA" />
              </div>

              <FormControl disabled={this.state.isShareThroughEmail ? false : true} variant="outlined" className='create-habit-customised-dropdown-menu'>
                <InputLabel htmlFor="name-disabled">Enter email</InputLabel>
                <div className=''>
                  <img src={emailIcon} alt='emailIcon' />
                  <Select value={this.state.shareMail} onChange={this.handleChangeShare} inputProps={{}}>
                    {
                      this.state.users ?
                      this.state.users.map(user => {
                        return <MenuItem value={user.emailIs}>{user.emailIs}</MenuItem>
                      }) : ''
                    }
                  </Select>
                </div>
              </FormControl>
            </div>
          </div>
        </div>
      </div>

      <div className='create-habit-footer-section'>
        <div>
          <Checkbox className='Save-as-an-Idea-checkbox' checked={this.state.saveTemplate} onChange={this.checkBoxHandlerTemplate} value={this.state.saveTemplate} />
          <span>Save as an Idea for future use</span>
        </div>

        <Button variant="contained" className="ch-submit-button" onClick={this.submitHandler}>
          Submit
          </Button>
      </div>
    </div>

  )


  render() {
    return (
      <Drawer anchor="right" onClick={this.closeCompanyDrawer} open={this.props.isOpenCreateHabit} className='create-ticket-drawer'>
        {this.sideList('right')}
      </Drawer>
    )
  }
}

export default CreateHabitDrawer;
