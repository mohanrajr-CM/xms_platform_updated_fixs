import gql from "graphql-tag";

const getBackLogData = gql`
    query GetBacklogHabit($pageNo:Int!)
    {
        getBacklogHabit(pageNo:$pageNo){
            id,
            habitTitle,
            habitDescription,
            startTime,
            progressPercent,
            statusId
        }
    }
`;

export const getHabitsBacklogData = (client, pageNo, callback) => {
    client
        .query({
            query:
                getBackLogData, variables: { pageNo: pageNo }
        })
        .then(res => {
            callback(res)
        })
}
const getTodayData = gql`
    query GetTodayHabit($pageNo:Int!)
    {
        getTodayHabit(pageNo:$pageNo){
            id,
            habitTitle,
            habitDescription
        }
    }
`;


export const getHabitsTodayData = (client, pageNo, callback) => {
    client
        .query({
            query:
                getTodayData, variables: { pageNo: pageNo }
        })
        .then(res => {
            callback(res)
        })
}


const getTomorrowHabits = gql`
    query GetTomorrowHabits($pageNo:Int!)
    {
        getTomorrowHabit(pageNo:$pageNo){
            id,
            habitTitle,
            habitDescription
        }
    }
`;
const GET_PROJECTS = gql`
{
    getAllProjects{
        id,
        projectName
    }
}
`;

function GetProjects(client, callback) {
    client
        .query({
            query:
                GET_PROJECTS
        })
        .then(res => {
            callback(res);
        })
}

export const getHabitTomorrowData = (client, pageNo, callback) => {
    client
        .query({
            query:
                getTomorrowHabits, variables: { pageNo: pageNo }
        })
        .then(res => {
            callback(res)
        })
}
export {
    GetProjects
}