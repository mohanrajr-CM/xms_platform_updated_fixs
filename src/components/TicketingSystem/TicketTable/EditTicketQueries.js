import gql from "graphql-tag"

const GET_DEPARTMENTS = gql`
       {
            getAllDepartments{
                id,
                departmentName
            }
        }
`;
const GET_COMPANIES = gql`
       {
        getAllCompany{
                id,
                companyName
            }
        }
`;
const GET_STATUS = gql`
       {
        getAllStatus{
                id,
                statusName
            }
        }
`;
const GET_PRIORITY = gql`
       {
        priorities{
                id,
                priorityname
            }
        }
`;
const GET_TEAM = gql`
       {
        getAllTeams{
                id,
                teamName
            }
        }
`;

const GET_PROJECTS = gql`
{
    getAllProjects{
        id,
        projectName
    }
}
`;

async function GetDepartments(client) {
    let result;
    await client
        .query({
            query:
                GET_DEPARTMENTS
        })
        .then(res => {
            // callback(res);
            result = res;
        })
    return result;
}

async function GetCompanies(client) {
    let result;
    await client
        .query({
            query:
                GET_COMPANIES
        })
        .then(res => {
            result = res;
        })
    return result;
}

async function GetStatus(client) {
    let result;
    await client
        .query({
            query:
                GET_STATUS
        })
        .then(res => {
            result = res;
        })
    return result;
}

async function GetPriority(client) {
    let result;
    await client
        .query({
            query:
                GET_PRIORITY
        })
        .then(res => {
            result = res;
        })
    return result;
}
async function GetTeams(client) {
    let result;
    await client
        .query({
            query:
                GET_TEAM
        })
        .then(res => {
            result = res;
        })
    return result;
}
async function GetProjects(client) {
    let result;
    await client
        .query({
            query:
                GET_PROJECTS
        })
        .then(res => {
            result = res;
        })
    return result;
}

// Edit ticket
const EDIT_TICKET = gql`
mutation UpdateTicket(
    $name: String, $ticket_description: String,
    $id:Int!,$company:Int!,$status:Int,
    $priority:Int,$tags:JSON,
    $manager:Int!,$assignedTo:Int!,
    $department:Int!,$team:Int!,$project:Int!,
  ) {
  updateTicket(
      id:$id,
      name: $name, 
      ticketDescription: $ticket_description,
      companyId:$company,
      statusId:$status,
      priorityId:$priority,
      tags:$tags,
      managerId:$manager,
      assignedToAgentId:$assignedTo,
      departmentId:$department,
      teamId:$team,
      projectId:$project,
  ) {
    id
    name
    ticketDescription
    userId
    ticketType
    priorityId
  }
}
`;

async function updateTicket(client, data) {
    console.log("DATA", data);
    let result;
    await client
        .mutate({
            mutation:
                EDIT_TICKET, variables: {

                    id: Number(data.id),
                    name: data.name,
                    ticket_description: data.ticketDescription,
                    company: Number(data.companyId),
                    status: Number(data.statusId),
                    priority: Number(data.priorityId),
                    tags: Number(data.tags),
                    ticketType: Number(data.ticketType),
                    manager: Number(data.managerId),
                    assignedTo: Number(data.assignedToAgentId),
                    department: Number(data.departmentId),
                    team: Number(data.teamId),
                    project: Number(data.projectId),
                    company: Number(data.companyName),
                    project:Number(data.priorityId)
                    // ticketType: data.ticketTypeName
                    // userId: data.userId,
                    // tenantId: data.tenantId,
                }
        })
        .then(res => {
            console.log("EDIT TICKET RESULT", res);
            result = res;
        })
    return result;
}

export {
    GetDepartments,
    GetCompanies,
    GetStatus,
    GetPriority,
    GetTeams,
    GetProjects,
    updateTicket
}
