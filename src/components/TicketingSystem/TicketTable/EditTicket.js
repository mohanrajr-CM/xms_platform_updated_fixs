import React from "react";
import Drawer from '@material-ui/core/Drawer';
import {
    GetDepartments, GetCompanies, GetStatus, GetPriority,
    GetTeams, GetProjects, updateTicket
} from "./EditTicketQueries";
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Dragula from 'react-dragula';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import "./TicketTable.scss";
import "./editTicket.scss";
import DrpDwnIcn from "../../../assets/icons/create-ticket/Icon ionic-md-arrow-dropdown.svg";
import Radio from '@material-ui/core/Radio';
import { withStyles } from '@material-ui/core/styles';

const StyledMenuItem = withStyles(theme => ({
    root: {
        // '&:focus': {
        //   backgroundColor: theme.palette.primary.main,
        //   '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        //     color: theme.palette.common.white,
        //   },
        // },
    },
}))(MenuItem);

let wrapperRef;
class EditTicket extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            ticketid: '',
            name: '',
            ticket_description: '',
            ticketTypeName: '',
            managerId: '',
            assignedToName: '',
            departmentName: '',
            teamName: '',
            projectName: '',
            contactName: '',
            userId: '',
            tenantId: '',

            companyName: '',
            statusName: '',
            priorityName: '',
            tagsName: [],

            departMentApiData: [],
            companyApiData: [],
            statusApiData: [],
            priorityApiData: [],
            teamApiData: [],
            projectApiData: [],

            // Added on 21st Nov
            isCompanyDropdown: false,
            cName: "Company Name",
            isStatusDropdown: false,
            sName: "Status",
            isPriorityDropdown: false,
            pName: "Priority",
            isTagsDropdown: false,
            tName: "Tags",
            isTypeDropdown: false,
            tyName: "Type",
            isManagerDropdown: false,
            mName: "Manager",
            isAssignDropdown: false,
            aName: "Assign to",
            isDepartmentDropdown: false,
            depName: "Department",
            isTeamDropdown: false,
            teName: "Team"
        }
    }

    componentDidMount() {
        // document.addEventListener('mousedown', this.handleClickOutsideBody);
        this.getDepartmentsData();
        this.getCompaniesData();
        this.getStatusData();
        this.getPriorityData();
        this.getTeamsData();
        this.getProjectsData();
    }

    handleUpdateTicket = async () => {
        const data = {
            id: this.props.ticketEData.id,
            name: this.state.name ? this.state.name : this.props.ticketEData.name,
            ticketDescription: this.state.ticket_description ? this.state.ticket_description : this.props.ticketEData.ticketDescription,
            // ticketTypeName:this.state.ticketTypeName ? this.state.ticketTypeName : this.props.ticketEData.ticketType,
            tenantId: this.state.tenantId ? this.state.tenantId : this.props.ticketEData.tenantId,
            userId: this.state.userId ? this.state.userId : this.props.ticketEData.userId,
            managerId: this.state.managerName ? this.state.managerName : this.props.ticketEData.managerId,
            statusId: this.state.statusName ? this.state.statusName : this.props.ticketEData.statusId,
            departmentId: this.state.departmentName ? this.state.departmentName : this.props.ticketEData.departmentId,
            teamId: this.state.teamName ? this.state.teamName : this.props.ticketEData.teamId,
            assignedToAgentId: this.state.assignedToName ? this.state.assignedToName : this.props.ticketEData.assignedToAgentId,
            priorityId: this.state.projectName ? this.state.projectName : this.props.ticketEData.projectId,
            companyName: this.state.companyName ? this.state.companyName : this.props.ticketEData.companyId
        }

        const result = await updateTicket(this.props.client, data)
        if (result) {
            this.props.thisObj.setState({ isEditDrawerOpen: false })
        }
    }

    getDepartmentsData = async () => {
        const result = await GetDepartments(this.props.client);
        if (result) {
            if (this.state.departMentApiData == '' || this.state.departMentApiData == undefined
                || this.state.departMentApiData == null) {
                this.setState({
                    departMentApiData: result.data.getAllDepartments
                });
            }
        }
    }
    getCompaniesData = async () => {
        const result = await GetCompanies(this.props.client);
        if (result) {
            if (this.state.companyApiData == '' || this.state.companyApiData == undefined
                || this.state.companyApiData == null) {
                this.setState({
                    companyApiData: result.data.getAllCompany
                });
            }
        }
    }
    getStatusData = async () => {
        const result = await GetStatus(this.props.client);
        if (result) {
            if (this.state.statusApiData == '' || this.state.statusApiData == undefined || this.state.statusApiData == null) {
                this.setState({
                    statusApiData: result.data.getAllStatus
                });
            }
        }
    }
    getPriorityData = async () => {
        const result = await GetPriority(this.props.client);
        if (result) {
            if (this.state.priorityApiData == '' || this.state.priorityApiData == undefined
                || this.state.priorityApiData == null) {
                this.setState({
                    priorityApiData: result.data.priorities
                });
            }
        }
    }

    getTeamsData = async () => {
        const result = await GetTeams(this.props.client);
        if (result) {
            if (this.state.teamApiData == '' || this.state.teamApiData == undefined || this.state.teamApiData == null) {
                this.setState({
                    teamApiData: result.data.getAllTeams
                });
            }
        }
    }

    getProjectsData = async () => {
        const result = await GetProjects(this.props.client);
        if (result) {
            if (this.state.projectApiData == '' || this.state.projectApiData == undefined
                || this.state.projectApiData == null) {
                this.setState({
                    projectApiData: result.data.getAllProjects
                })
            }
        }
    }

    toggleDrawer = (side, open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        this.setState({ ...this.state, [side]: open });
    };

    closeEditDrawerHandler = () => {
        this.props.thisObj.setState({ isEditDrawerOpen: false })
    }

    handleChangeContact = event => {
        this.setState({ contactName: event.target.value, });
    }
    handleChangeProject = event => {
        this.setState({ projectName: event.target.value, });
    }
    handleChangeText = event => {
        this.setState({ name: event.target.value });
    };

    handleChangeTextarea = event => {
        this.setState({ ticket_description: event.target.value });
    };



    // Added on 21st Nov

    // handleClickOutsideBody = async (event) => {
    //     if (wrapperRef && !wrapperRef.contains(event.target)) {
    //         await this.setState({
    //             isCompanyDropdown: false,
    //         })
    //     }
    // }

    setWrapperRef = (node) => wrapperRef = node;

    //Company 
    handleChangeCompany = (event, cdata) => {
        this.setState({
            companyName: event.target.value,
            cName: cdata.companyName,
            isCompanyDropdown: false,
            isStatusDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        });
    }
    showCompanyDropdown = () => {
        this.setState({
            isCompanyDropdown: !this.state.isCompanyDropdown,
            isStatusDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        })
    }
    // Status
    handleChangeStatus = (event, sdata) => {
        this.setState({
            statusName: event.target.value,
            sName: sdata.statusName,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        });
    }
    showStatusDropdown = () => {
        this.setState({
            isStatusDropdown: !this.state.isStatusDropdown,
            isCompanyDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        })
    }

    // Priority
    handleChangePriority = (event, pdata) => {
        this.setState({
            priorityName: event.target.value,
            pName: pdata.priorityname,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        });
    }
    showPriorityDropdown = () => {
        this.setState({
            isPriorityDropdown: !this.state.isPriorityDropdown,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        })
    }

    // Tags
    handleChangeTags = (event, tdata) => {
        this.setState({
            tagsName: event.target.value,
            tName: tdata.priorityname,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        });
    }
    showTagsDropdown = () => {
        this.setState({
            isTagsDropdown: !this.state.isTagsDropdown,
            isPriorityDropdown: false,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        })
    }

    // Type
    handleChangeTicketType = (event, tydata) => {
        this.setState({
            ticketTypeName: event.target.value,
            tyName: tydata.priorityname,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        });
    }
    showTypeDropdown = () => {
        this.setState({
            isTypeDropdown: !this.state.isTypeDropdown,
            isTagsDropdown: false,
            isPriorityDropdown: false,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        })
    }

    // manager
    handleChangeManager = (event, mdata) => {
        this.setState({
            managerName: event.target.value,
            mName: mdata.firstName,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        });
    }
    showManagerDropdown = () => {
        this.setState({
            isManagerDropdown: !this.state.isManagerDropdown,
            isTypeDropdown: false,
            isTagsDropdown: false,
            isPriorityDropdown: false,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        })
    }

    // Assign to
    handleChangeAssignedTo = (event, adata) => {
        this.setState({
            assignedToName: event.target.value,
            aName: adata.firstName,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        });
    }
    showAssignDropdown = () => {
        this.setState({
            isAssignDropdown: !this.state.isAssignDropdown,
            isManagerDropdown: false,
            isTypeDropdown: false,
            isTagsDropdown: false,
            isPriorityDropdown: false,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        })
    }

    // Departments
    handleChangeDepartment = (event, adata) => {
        this.setState({
            departmentName: event.target.value,
            depName: adata.departmentName,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        });
    }
    showDepartmentDropdown = () => {
        this.setState({
            isDepartmentDropdown: !this.state.isDepartmentDropdown,
            isAssignDropdown: false,
            isManagerDropdown: false,
            isTypeDropdown: false,
            isTagsDropdown: false,
            isPriorityDropdown: false,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isTeamDropdown:false
        })
    }

    // Teams
    handleChangeTeam = (event, adata) => {
        this.setState({
            teamName: event.target.value,
            teName: adata.teamName,
            isStatusDropdown: false,
            isCompanyDropdown: false,
            isPriorityDropdown: false,
            isTagsDropdown: false,
            isTypeDropdown: false,
            isManagerDropdown: false,
            isAssignDropdown: false,
            isDepartmentDropdown:false,
            isTeamDropdown:false
        });
    }
    showTeamDropdown = () => {
        this.setState({
            isTeamDropdown: !this.state.isTeamDropdown,
            isAssignDropdown: false,
            isManagerDropdown: false,
            isTypeDropdown: false,
            isTagsDropdown: false,
            isPriorityDropdown: false,
            isStatusDropdown: false,
            isCompanyDropdown: false,
        })
    }
    // Added on 21st ends
    editSidePanel = (side, ticket, index) => (
        <div
            // style={{ width: '410px' }}
            className="edit-container"
            role="presentation"
        >
            <div className="edit-tkt-sidebar-container">
                <div className="edit-tkt-sidebar-header">
                    <div className="edit-tkt-heading">
                        EDIT TICKET
                        <span>
                            ({this.props.ticketEData ? this.props.ticketEData.id : ''})
                        </span>
                    </div>
                </div>
                <div>
                    {/*<div className="edit-ticket-body-input-base-two">
                       <div className="ticket-type-container">
                             <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Company</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        // value={this.props.ticketEData && this.props.ticketEData? this.props.ticketEData.company:''}
                                        value={this.state.companyName}
                                        onChange={this.handleChangeCompany}
                                        disableUnderline
                                        name='companyName'
                                        inputProps={{
                                            companyName: 'companyName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        {
                                            this.state.companyApiData ?
                                                this.state.companyApiData.map(cData => {
                                                    return <MenuItem value={cData.id}>{cData.companyName}</MenuItem>
                                                }) : ''
                                        }
                                    </Select>
                                </div>
                            </FormControl> 
                        </div>
                        <div className="ticket-title-container">
                            <TextField
                                label="SUBJECT"
                                id="margin-none"
                                variant="outlined"
                                // placeholder={recordData && recordData.recordData ? recordData.recordData.name : ''}
                                name='name'
                                defaultValue={this.props.ticketEData ? this.props.ticketEData.name : ''}
                                onChange={(e) => this.handleChangeText(e)}
                            />

                        </div>
                    </div>*/}

                    {/* Added on 21st Nov */}

                    <div className="edit-ticket-drawer-ticket-comp-description d-flex justify-space-between">
                        <div className="edit-ticket-drawer-ticket-company">
                            <div className='menu-comp-container'>
                                <div className='edit-drawer-container-comp d-flex'
                                    onClick={this.showCompanyDropdown}
                                >
                                    <div className='selected-item-container'>
                                        <p>
                                            {
                                                this.state.cName
                                            }
                                        </p>
                                    </div>
                                    <div className="ticket-drp-dwn-img">
                                        <img src={DrpDwnIcn}
                                            onClick={this.showCompanyDropdown}
                                        />
                                    </div>

                                </div>

                                <div className={`ticket-type-dropdown-menu-container 
                                    ${this.state.isCompanyDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                                    ref={this.setWrapperRef}
                                >
                                    {this.state.companyApiData ? this.state.companyApiData.map((cData, index) =>
                                        <StyledMenuItem className="customized-ticket-source"
                                            onClose={this.handleClose}
                                        >
                                            <div className="create-ticket-one-tckt-typ d-flex">
                                                <div className="create-ticket-tckt-img-text d-flex">
                                                    {/* <img src={cData.icon}></img> */}
                                                    <p>{cData.companyName}</p>
                                                </div>

                                                <div className="create-ticket-src-radio">
                                                    <Radio
                                                        checked={cData.id == this.state.companyName}
                                                        value={cData.id}
                                                        name="ticketTypeId"
                                                        color="primary"
                                                        inputProps={{ 'aria-label': '' }}
                                                        onChange={(e) => this.handleChangeCompany(e, cData)}
                                                    />
                                                </div>
                                            </div>

                                        </StyledMenuItem>
                                    ) : null}

                                </div>
                            </div>
                        </div>
                        <div className="ticket-title-container">
                            <TextField
                                label="SUBJECT"
                                id="margin-none"
                                variant="outlined"
                                // placeholder={recordData && recordData.recordData ? recordData.recordData.name : ''}
                                name='name'
                                // defaultValue={this.props.ticketEData ? this.props.ticketEData.name : ''}
                                onChange={(e) => this.handleChangeText(e)}
                            />
                        </div>
                    </div>

                    {/* Added on 21st Nov ends */}
                    {/* <div className="edit-ticket-body-input-base-two">
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Status</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        value={this.props.ticketEData ? this.props.ticketEData.statusId : ''}
                                        value={this.state.statusName}
                                        onChange={this.handleChangeStatus}
                                        disableUnderline
                                        name='statusName'
                                        inputProps={{
                                            companyName: 'statusName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        {
                                            this.state.statusApiData ? this.state.statusApiData.map(sData => {
                                                return <MenuItem value={sData.id}>{sData.statusName}</MenuItem>
                                            }) : ''
                                        }
                                    </Select>
                                </div>
                            </FormControl>
                        </div>
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Priority</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        value={this.props.ticketEData ? this.props.ticketEData.priorityId : ''}
                                        value={this.state.priorityName}
                                        onChange={this.handleChangePriority}
                                        disableUnderline
                                        name='priorityName'
                                        inputProps={{
                                            companyName: 'priorityName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        {
                                            this.state.priorityApiData ?
                                                this.state.priorityApiData.map(pData => {
                                                    return <MenuItem value={pData.id}>{pData.priorityname}</MenuItem>
                                                }) : ''
                                        }
                                    </Select>
                                </div>
                            </FormControl>
                        </div>
                    </div> */}

                    {/* Added on 21st Status and priority section */}
                    <div className="edit-ticket-drawer-ticket-status-priority d-flex justify-space-between">
                        <div className="edit-ticket-drawer-ticket-status">
                            <div className='menu-status-container'>
                                <div className='edit-drawer-container-status d-flex'
                                    onClick={this.showStatusDropdown}
                                >
                                    <div className='selected-item-container'>
                                        <p>
                                            {
                                                this.state.sName
                                            }
                                        </p>
                                    </div>
                                    <div className="ticket-drp-dwn-img">
                                        <img src={DrpDwnIcn}
                                            onClick={this.showStatusDropdown}
                                        />
                                    </div>

                                </div>

                                <div className={`ticket-type-dropdown-menu-container 
                                    ${this.state.isStatusDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                                    ref={this.setWrapperRef}
                                >
                                    {this.state.statusApiData ? this.state.statusApiData.map((sData, index) =>
                                        <StyledMenuItem className="customized-ticket-source"
                                            onClose={this.handleClose}
                                        >
                                            <div className="create-ticket-one-tckt-typ d-flex">
                                                <div className="create-ticket-tckt-img-text d-flex">
                                                    {/* <img src={cData.icon}></img> */}
                                                    <p>{sData.statusName}</p>
                                                </div>

                                                <div className="create-ticket-src-radio">
                                                    <Radio
                                                        checked={sData.id == this.state.statusName}
                                                        value={sData.id}
                                                        name="ticketTypeId"
                                                        color="primary"
                                                        inputProps={{ 'aria-label': '' }}
                                                        onChange={(e) => this.handleChangeStatus(e, sData)}
                                                    />
                                                </div>
                                            </div>

                                        </StyledMenuItem>
                                    ) : null}

                                </div>
                            </div>
                        </div>
                        <div className="edit-ticket-drawer-ticket-priority">
                            <div className='menu-priority-container'>
                                <div className='priority-container'
                                    onClick={this.showPriorityDropdown}
                                >
                                    <div className='selected-item-container'>
                                        <p>
                                            {
                                                this.state.pName
                                            }
                                        </p>
                                    </div>
                                    <div className="ticket-drp-dwn-img">
                                        <img src={DrpDwnIcn}
                                            onClick={this.showPriorityDropdown}
                                        />
                                    </div>

                                </div>

                                <div className={`priority-dropdown-menu-container 
                                    ${this.state.isPriorityDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                                    ref={this.setWrapperRef}
                                >
                                    {this.state.priorityApiData ? this.state.priorityApiData.map((pData, index) =>
                                        <StyledMenuItem className="customized-ticket-source"
                                            onClose={this.handleClose}
                                        >
                                            <div className="create-ticket-one-tckt-typ d-flex">
                                                <div className="create-ticket-tckt-img-text d-flex">
                                                    {/* <img src={cData.icon}></img> */}
                                                    <p>{pData.priorityname}</p>
                                                </div>

                                                <div className="create-ticket-src-radio">
                                                    <Radio
                                                        checked={pData.id == this.state.priorityName}
                                                        value={pData.id}
                                                        name="ticketTypeId"
                                                        color="primary"
                                                        inputProps={{ 'aria-label': '' }}
                                                        onChange={(e) => this.handleChangePriority(e, pData)}
                                                    />
                                                </div>
                                            </div>

                                        </StyledMenuItem>
                                    ) : null}

                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Added on 21st Status and priority section ends */}
                    <div className="edit-ticket-text-area">
                        <TextareaAutosize
                            label="DESCRIPTION"
                            id="margin-none"
                            variant="outlined"
                            aria-label="minimum height"
                            name="ticket_description"
                            rows={3} placeholder="Ticket description"
                            onChange={(e) => this.handleChangeTextarea(e)}
                        >
                            {this.props.ticketEData ? this.props.ticketEData.ticketDescription : ''}
                        </TextareaAutosize>
                    </div>
                    {/* <div className="edit-ticket-body-input-base-two">
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Tags</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        multiple
                                        // value={recordData && recordData.recordData? recordData.recordData.company:''}
                                        value={this.state.tagsName}
                                        onChange={this.handleChangeTags}
                                        disableUnderline
                                        name='tagsName'
                                        inputProps={{
                                            companyName: 'tagsName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        <MenuItem value="1">Risk</MenuItem>
                                        <MenuItem value="2">Critical Customer</MenuItem>
                                        <MenuItem value="3">Phase1</MenuItem>
                                        <MenuItem value="4">Technical</MenuItem>
                                    </Select>
                                </div>
                            </FormControl>

                        </div>
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Type</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        // value={recordData && recordData.recordData? recordData.recordData.company:''}
                                        value={this.state.ticketTypeName}
                                        onChange={this.handleChangeTicketType}
                                        disableUnderline
                                        name='ticketTypeName'
                                        inputProps={{
                                            companyName: 'ticketTypeName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        <MenuItem value="1">Service Request</MenuItem>
                                        <MenuItem value="2">Incident</MenuItem>
                                        <MenuItem value="3">Problem</MenuItem>
                                    </Select>
                                </div>
                            </FormControl>
                        </div>
                    </div> */}
                    {/* Added on Nov 21st tags and type section*/}
                    <div className="edit-ticket-drawer-ticket-tags-type d-flex justify-space-between">
                        <div className="edit-ticket-drawer-ticket-tags">
                            <div className='menu-tags-container'>
                                <div className='edit-drawer-container-tags d-flex'
                                    onClick={this.showTagsDropdown}
                                >
                                    <div className='selected-item-container'>
                                        <p>
                                            {
                                                this.state.tName
                                            }
                                        </p>
                                    </div>
                                    <div className="ticket-drp-dwn-img">
                                        <img src={DrpDwnIcn}
                                            onClick={this.showTagsDropdown}
                                        />
                                    </div>

                                </div>

                                <div className={`ticket-type-dropdown-menu-container 
                                    ${this.state.isTagsDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                                    ref={this.setWrapperRef}
                                >
                                    {/* {this.state.statusApiData ? this.state.statusApiData.map((sData, index) =>
                                        <StyledMenuItem className="customized-ticket-source"
                                            onClose={this.handleClose}
                                        >
                                            <div className="create-ticket-one-tckt-typ d-flex">
                                                <div className="create-ticket-tckt-img-text d-flex">
                                                    <p>{sData.statusName}</p>
                                                </div>

                                                <div className="create-ticket-src-radio">
                                                    <Radio
                                                        checked={sData.id == this.state.statusName}
                                                        value={sData.id}
                                                        name="ticketTypeId"
                                                        color="primary"
                                                        inputProps={{ 'aria-label': '' }}
                                                        onChange={(e) => this.handleChangeStatus(e, sData)}
                                                    />
                                                </div>
                                            </div>

                                        </StyledMenuItem>
                                    ) : null} */}

                                </div>
                            </div>
                        </div>
                        <div className="edit-ticket-drawer-ticket-type">
                            <div className='menu-type-container'>
                                <div className='type-container'
                                    onClick={this.showTypeDropdown}
                                >
                                    <div className='selected-item-container'>
                                        <p>
                                            {
                                                this.state.tyName
                                            }
                                        </p>
                                    </div>
                                    <div className="ticket-drp-dwn-img">
                                        <img src={DrpDwnIcn}
                                            onClick={this.showTypeDropdown}
                                        />
                                    </div>

                                </div>

                                <div className={`type-dropdown-menu-container 
                                    ${this.state.isTypeDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                                    ref={this.setWrapperRef}
                                >
                                    {/* {this.state.priorityApiData ? this.state.priorityApiData.map((pData, index) =>
                                        <StyledMenuItem className="customized-ticket-source"
                                            onClose={this.handleClose}
                                        >
                                            <div className="create-ticket-one-tckt-typ d-flex">
                                                <div className="create-ticket-tckt-img-text d-flex">
                                                    <p>{pData.priorityname}</p>
                                                </div>

                                                <div className="create-ticket-src-radio">
                                                    <Radio
                                                        checked={pData.id == this.state.priorityName}
                                                        value={pData.id}
                                                        name="ticketTypeId"
                                                        color="primary"
                                                        inputProps={{ 'aria-label': '' }}
                                                        onChange={(e) => this.handleChangePriority(e, pData)}
                                                    />
                                                </div>
                                            </div>

                                        </StyledMenuItem>
                                    ) : null} */}

                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Added on Nov 21st tags and types section ends */}

                    {/* <div className="edit-ticket-body-input-base-two">
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Manager</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        // value={recordData && recordData.recordData? recordData.recordData.company:''}
                                        value={this.state.managerName}
                                        onChange={this.handleChangeManager}
                                        disableUnderline
                                        name='managerName'
                                        inputProps={{
                                            companyName: 'managerName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        <MenuItem value="1">Sugata Maji</MenuItem>
                                        <MenuItem value="2">Mohan</MenuItem>
                                    </Select>
                                </div>
                            </FormControl>
                        </div>
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Assigned To</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        // value={recordData && recordData.recordData? recordData.recordData.company:''}
                                        value={this.state.assignedToName}
                                        onChange={this.handleChangeAssignedTo}
                                        disableUnderline
                                        name='assignedToName'
                                        inputProps={{
                                            companyName: 'assignedToName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        <MenuItem value="1">John Doe</MenuItem>
                                        <MenuItem value="2">Sugata Maji</MenuItem>
                                        <MenuItem value="3">Mohan</MenuItem>
                                    </Select>
                                </div>
                            </FormControl>
                        </div>
                    </div> */}

                    {/* Added on Nov 21st manager and assign to section */}
                    <div className="edit-ticket-drawer-ticket-manager-assign d-flex justify-space-between">
                        <div className="edit-ticket-drawer-ticket-manager">
                            <div className='menu-manager-container'>
                                <div className='edit-drawer-container-manager d-flex'
                                    onClick={this.showManagerDropdown}
                                >
                                    <div className='selected-item-container'>
                                        <p>
                                            {
                                                this.state.mName
                                            }
                                        </p>
                                    </div>
                                    <div className="ticket-drp-dwn-img">
                                        <img src={DrpDwnIcn}
                                            onClick={this.showManagerDropdown}
                                        />
                                    </div>

                                </div>

                                <div className={`ticket-manager-dropdown-menu-container 
                                    ${this.state.isManagerDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                                    ref={this.setWrapperRef}
                                >
                                    {/* {this.state.statusApiData ? this.state.statusApiData.map((sData, index) =>
                                        <StyledMenuItem className="customized-ticket-source"
                                            onClose={this.handleClose}
                                        >
                                            <div className="create-ticket-one-tckt-typ d-flex">
                                                <div className="create-ticket-tckt-img-text d-flex">
                                                    <p>{sData.statusName}</p>
                                                </div>

                                                <div className="create-ticket-src-radio">
                                                    <Radio
                                                        checked={sData.id == this.state.statusName}
                                                        value={sData.id}
                                                        name="ticketTypeId"
                                                        color="primary"
                                                        inputProps={{ 'aria-label': '' }}
                                                        onChange={(e) => this.handleChangeStatus(e, sData)}
                                                    />
                                                </div>
                                            </div>

                                        </StyledMenuItem>
                                    ) : null} */}

                                </div>
                            </div>
                        </div>
                        <div className="edit-ticket-drawer-ticket-assign">
                            <div className='menu-assign-container'>
                                <div className='assign-container'
                                    onClick={this.showAssignDropdown}
                                >
                                    <div className='selected-item-container'>
                                        <p>
                                            {
                                                this.state.aName
                                            }
                                        </p>
                                    </div>
                                    <div className="ticket-drp-dwn-img">
                                        <img src={DrpDwnIcn}
                                            onClick={this.showAssignDropdown}
                                        />
                                    </div>

                                </div>

                                <div className={`assign-dropdown-menu-container 
                                    ${this.state.isAssignDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                                    ref={this.setWrapperRef}
                                >
                                    {/* {this.state.priorityApiData ? this.state.priorityApiData.map((pData, index) =>
                                        <StyledMenuItem className="customized-ticket-source"
                                            onClose={this.handleClose}
                                        >
                                            <div className="create-ticket-one-tckt-typ d-flex">
                                                <div className="create-ticket-tckt-img-text d-flex">
                                                    <p>{pData.priorityname}</p>
                                                </div>

                                                <div className="create-ticket-src-radio">
                                                    <Radio
                                                        checked={pData.id == this.state.priorityName}
                                                        value={pData.id}
                                                        name="ticketTypeId"
                                                        color="primary"
                                                        inputProps={{ 'aria-label': '' }}
                                                        onChange={(e) => this.handleChangePriority(e, pData)}
                                                    />
                                                </div>
                                            </div>

                                        </StyledMenuItem>
                                    ) : null} */}

                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Added on Nov 21st manager and assign to section ends */}


                    {/* <div className="edit-ticket-body-input-base-two">
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Department</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        // value={recordData && recordData.recordData? recordData.recordData.company:''}
                                        value={this.state.departmentName}
                                        onChange={this.handleChangeDepartment}
                                        disableUnderline
                                        name='departmentName'
                                        inputProps={{
                                            companyName: 'departmentName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        {
                                            this.state.departMentApiData ?
                                                this.state.departMentApiData.map(dData => {
                                                    return <MenuItem value={dData.id}>
                                                        {dData.departmentName}
                                                    </MenuItem>
                                                }) : ''
                                        }
                                    </Select>
                                </div>
                            </FormControl>
                        </div>
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Team</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        // value={recordData && recordData.recordData? recordData.recordData.company:''}
                                        value={this.state.teamName}
                                        onChange={this.handleChangeTeam}
                                        disableUnderline
                                        name='teamName'
                                        inputProps={{
                                            companyName: 'teamName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        {
                                            this.state.teamApiData ? this.state.teamApiData.map(tData => {
                                                return <MenuItem value={tData.id}>{tData.teamName}</MenuItem>
                                            }) : ''
                                        }
                                    </Select>
                                </div>
                            </FormControl>
                        </div>
                    </div> */}

                    {/* Added on Nov 20st Department and Team*/}

                    <div className="edit-ticket-drawer-ticket-manager-assign d-flex justify-space-between">
                        <div className="edit-ticket-drawer-ticket-manager">
                            <div className='menu-manager-container'>
                                <div className='edit-drawer-container-manager d-flex'
                                    onClick={this.showDepartmentDropdown}
                                >
                                    <div className='selected-item-container'>
                                        <p>
                                            {
                                                this.state.depName
                                            }
                                        </p>
                                    </div>
                                    <div className="ticket-drp-dwn-img">
                                        <img src={DrpDwnIcn}
                                            onClick={this.showDepartmentDropdown}
                                        />
                                    </div>

                                </div>

                                <div className={`ticket-manager-dropdown-menu-container 
                                    ${this.state.isDepartmentDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                                    ref={this.setWrapperRef}
                                >
                                    {this.state.departMentApiData ? this.state.departMentApiData.map((dData, index) =>
                                        <StyledMenuItem className="customized-ticket-source"
                                            onClose={this.handleClose}
                                        >
                                            <div className="create-ticket-one-tckt-typ d-flex">
                                                <div className="create-ticket-tckt-img-text d-flex">
                                                    <p>{dData.departmentName}</p>
                                                </div>

                                                <div className="create-ticket-src-radio">
                                                    <Radio
                                                        checked={dData.id == this.state.departmentName}
                                                        value={dData.id}
                                                        name="ticketTypeId"
                                                        color="primary"
                                                        inputProps={{ 'aria-label': '' }}
                                                        onChange={(e) => this.handleChangeDepartment(e, dData)}
                                                    />
                                                </div>
                                            </div>

                                        </StyledMenuItem>
                                    ) : null}

                                </div>
                            </div>
                        </div>
                        <div className="edit-ticket-drawer-ticket-assign">
                            <div className='menu-assign-container'>
                                <div className='assign-container'
                                    onClick={this.showTeamDropdown}
                                >
                                    <div className='selected-item-container'>
                                        <p>
                                            {
                                                this.state.teName
                                            }
                                        </p>
                                    </div>
                                    <div className="ticket-drp-dwn-img">
                                        <img src={DrpDwnIcn}
                                            onClick={this.showTeamDropdown}
                                        />
                                    </div>

                                </div>

                                <div className={`assign-dropdown-menu-container 
                                    ${this.state.isTeamDropdown ? 'ticket-type-open-dropdown' : 'close-dropdown'}`}
                                    ref={this.setWrapperRef}
                                >
                                    {this.state.teamApiData ? this.state.teamApiData.map((tData, index) =>
                                        <StyledMenuItem className="customized-ticket-source"
                                            onClose={this.handleClose}
                                        >
                                            <div className="create-ticket-one-tckt-typ d-flex">
                                                <div className="create-ticket-tckt-img-text d-flex">
                                                    <p>{tData.teamName}</p>
                                                </div>

                                                <div className="create-ticket-src-radio">
                                                    <Radio
                                                        checked={tData.id == this.state.teamName}
                                                        value={tData.id}
                                                        color="primary"
                                                        inputProps={{ 'aria-label': '' }}
                                                        onChange={(e) => this.handleChangeTeam(e, tData)}
                                                    />
                                                </div>
                                            </div>

                                        </StyledMenuItem>
                                    ) : null}

                                </div>
                            </div>
                        </div>
                    </div>

                    {/* Added on Nov 21st Department and Team ends */}
                    <div className="edit-ticket-body-input-base-two">
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Project</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        // value={recordData && recordData.recordData? recordData.recordData.company:''}
                                        value={this.state.projectName}
                                        onChange={this.handleChangeProject}
                                        disableUnderline
                                        name='projectName'
                                        inputProps={{
                                            companyName: 'projectName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        {
                                            this.state.projectApiData ? this.state.projectApiData.map(pData => {
                                                return <MenuItem value={pData.id}>{pData.projectName}</MenuItem>
                                            }) : ''
                                        }
                                    </Select>
                                </div>
                            </FormControl>
                        </div>
                        <div className="ticket-type-container">
                            <FormControl>
                                <InputLabel htmlFor="demo-controlled-open-select">Contact</InputLabel>
                                <div className="edit-ticket-menu-item">
                                    <Select
                                        // value={recordData && recordData.recordData? recordData.recordData.company:''}
                                        value={this.state.contactName}
                                        onChange={this.handleChangeContact}
                                        disableUnderline
                                        name='contactName'
                                        inputProps={{
                                            companyName: 'contactName',
                                            id: 'age-native-simple',
                                        }}
                                    >
                                        <MenuItem value="1">Ryan Pazos</MenuItem>
                                        <MenuItem value="2">Mohan Raj</MenuItem>
                                    </Select>
                                </div>
                            </FormControl>
                        </div>
                    </div>
                    <div style={{ border: '0.05rem solid #d6d5d5', margin: '5% 4% 1%' }}></div>
                    <div className="edit-tkt-footer">
                        <div>
                            <Button
                                variant="outlined"
                                className="cancel-button"
                                onClick={this.closeEditDrawerHandler}
                                onKeyDown={this.closeEditDrawerHandler}
                            >
                                cancel
                            </Button>
                        </div>
                        <div>
                            <Button
                                size="large"
                                className="update-button"
                                onClick={(e) => this.handleUpdateTicket(e)}
                            >
                                Update
                            </Button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
    render() {
        return (
            <div className="edit-main-container">
                <Drawer
                    anchor="right"
                    open={this.props.isEditDrawerOpen}
                    onClose={this.toggleDrawer('right', false)}
                    className="tckt-lst-edit-drawer"
                >
                    <div className="editdrawer d-flex">
                        <div className="drawer-full-width" onClick={this.closeEditDrawerHandler}></div>
                        {this.editSidePanel("right")}
                    </div>
                </Drawer>
            </div>
        )
    }
}

export default EditTicket;