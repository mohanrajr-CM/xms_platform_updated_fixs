import React from 'react'
import './CreateTaskDrawer.scss';
import Drawer from '@material-ui/core/Drawer';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import DatePicker from 'react-date-picker';
import moment from 'moment';
import Button from '@material-ui/core/Button';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { CirclePicker, TwitterPicker } from 'react-color'
import { withStyles } from '@material-ui/core/styles';
import { Scrollbars } from 'react-custom-scrollbars';
import dropdownIcon from '../../assets/icons/01-10-2019/Icon ionic-md-arrow-dropdown.svg'
import colorPallet from '../../assets/create-habit/Icon material-color-lens.svg'
import closeCreateHabitIcon from '../../assets/create-habit/Icon ionic-ios-close.svg'
import emailIcon from '../../assets/icons/01-10-2019/Icon feather-mail.svg'
import profileIcon from '../../assets/profile.png'
import decrementIcon from '../../assets/icons/SVG/Group 11399.svg'
import estimatedTimeIcon from '../../assets/icons/SVG/Icon material-access-time.svg'
import tagsIcon from '../../assets/icons/SVG/Icon awesome-tags.svg'
import addNewIcon from '../../assets/icons/SVG/Group 11382.svg';
import InputBase from '@material-ui/core/InputBase';
import SearchTagAdd from "../../assets/icons/create-ticket/Group 11349.svg";
import ClrPckrTray from "../../assets/icons/create-ticket/Icon ionic-ios-color-palette.svg";
import LowIcon from "../../assets/icons/create-ticket/Rectangle 261.svg";
import HighIcon from "../../assets/icons/create-ticket/Rectangle 242.svg";
import CriticalIcon from "../../assets/icons/create-ticket/Rectangle 643.svg";
import DrpDwnIcn from "../../assets/icons/create-ticket/Icon ionic-md-arrow-dropdown.svg";
import Radio from '@material-ui/core/Radio';
import MediumIcon from "../../assets/icons/create-ticket/Path 887.svg";
import linkToTaskIcon from '../../assets/icons/SVG/Group11262.svg'
import linkToProjectIcon from '../../assets/icons/SVG/Group-11374.svg'
import linkToTicketIcon from '../../assets/icons/SVG/Group-11260.svg'
import linkToObjectiveIcon from '../../assets/icons/SVG/Icon-feather-target.svg'
import selectLinkToIcon from '../../assets/icons/SVG/Icon-feather-link.svg'
import linkToRelatedIcon from '../../assets/icons/SVG/Icon-ionic-ios-options.svg'
import DropDwnIcon from "../../assets/icons/SVG/Icon ionic-ios-arrow-down.svg";
import SearchIcon from '@material-ui/icons/Search';
import CompanyLogo from "../../assets/icons/01-10-2019/company-logo.svg";
import { baseUrl } from "../../constants";
import axios from "axios";

import CompanyEditIcon from "../../assets/icons/SVG/feather-edit.svg";
import EditCompanyDetails from '../../components/TicketingSystem/CreateTicket/EditCompanyDetails/EditCompanyDetails';
import removeAttachedFile from '../../assets/icons/create-ticket/Group 11407.svg'
import dpfIcon from '../../assets/icons/SVG/337946.png'

const fileIcons = [{pdf: dpfIcon}]
const imageFormats = ['jpg', 'jpeg', 'jpg', 'bmp', 'png', 'svg'];
const fileFormats = ['doc', 'docx', 'odt', 'txt', 'pdf', 'ppt', 'pptx'];

const quickCreateIcons = [
  {id: 0, icon: '', title: 'File attachment is not working'},
  {id: 1, icon: '', title: 'Getting error when doing drag-drop..'},
  {id: 2, icon: '', title: 'File attachment is not working'},
  {id: 3, icon: '', title: 'User permission issues'},
  {id: 4, icon: '', title: 'Error popup is showing to create a..'},
  {id: 5, icon: '', title: 'File attachment is not working'},
  {id: 1, icon: '', title: 'Getting error when doing drag-drop..'},
  {id: 2, icon: '', title: 'File attachment is not working'},
  {id: 3, icon: '', title: 'User permission issues'},
  {id: 4, icon: '', title: 'Error popup is showing to create a..'},
]

let time = ['12:00 AM', '1:00 AM', '2:00 AM', '3:00 AM', '4:00 AM', '5:00 AM', '6:00 AM', '7:00 AM', '8:00 AM', '9:00 AM', '10:00 AM', '11:00 AM', '12:00 PM', '1:00 PM', '2:00 PM', '3:00 PM', '4:00 PM', '5:00 PM', '6:00 PM', '7:00 PM', '8:00 PM', '9:00 PM', '10:00 PM', '11:00 PM']
const selectLinkToExisting  = [
  {id: 0, title: 'Tasks', icon: linkToTaskIcon},
  {id: 1, title: 'Projects', icon: linkToProjectIcon},
  {id: 2, title: 'Tickets', icon: linkToTicketIcon},
  {id: 3, title: 'Objectives', icon: linkToObjectiveIcon},
]
const weekDaysBtns = [
  {btnId: 0, isActive: false, btnName: 'SUN', },
  {btnId: 1, isActive: false, btnName: 'MON', },
  {btnId: 2, isActive: false, btnName: 'TUE', },
  {btnId: 3, isActive: false, btnName: 'WED', },
  {btnId: 4, isActive: false, btnName: 'THU', },
  {btnId: 5, isActive: false, btnName: 'FRI', },
  {btnId: 6, isActive: false, btnName: 'SAT', },
]
const assignToData =[
  {id: 0, name: 'John Dan', profile: ''},
  {id: 1, name: 'John Doe', profile: ''},
  {id: 2, name: 'John pazos', profile: ''},
  {id: 3, name: 'Mark John', profile: ''},
  {id: 4, name: 'John Dan', profile: ''},
  {id: 5, name: 'John Doe', profile: ''},
  {id: 6, name: 'John pazos', profile: ''},
  {id: 7, name: 'Mark John', profile: ''},
]
const tagsData = [
  {id: 0, title: 'Risk', },
  {id: 1, title: 'Critical Customer', },
  {id: 2, title: 'Phase1', },
  {id: 3, title: 'Technical', },
]
const TicketType = [
  {id: 1, icon: LowIcon, title: "Service request"},
  {id: 2, icon: HighIcon, title: "Incident"},
  {id: 3, icon: CriticalIcon, title: "Problem"},
]
const priorities = [
  {id: 1, icon: CriticalIcon, title: "Critical"},
  {id: 2, icon: HighIcon, title: "High"},
  {id: 3, icon: MediumIcon, title: "Medium"},
  {id: 4, icon: LowIcon, title: "Low"},
]
var linkRelatedRecords = [
  {id: 0, title: 'Project Management'},
  {id: 1, title: 'Metrik1 - marketing'},
  {id: 2, title: 'UI/UX design'},
  {id: 3, title: 'R&D on product1'},
  {id: 4, title: 'HR & Recruitment'},
  {id: 5, title: 'Project Management'},
  {id: 6, title: 'Project Management'},
]

const StyledMenuItem = withStyles(theme => ({
  root: {
    // '&:focus': {
    //   backgroundColor: theme.palette.primary.main,
    //   '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
    //     color: theme.palette.common.white,
    //   },
    // },
  },
}))(MenuItem);
const color = ['#feb1b2', '#7ac9ff', '#ffc089', '#41e590', '#ea5455', '#c8c8c8', '#656565', '#ecf1f9', '#7AC9FF']
const colors = ['#1abc9c', '#17a085', '#2ecc71', '#27ae60', '#3498db', '#2980b9', '#9b59b6', '#8e44ad', '#34495e', '#2c3e50', '#f1c40e', '#f39c12', '#d35400', '#e74c3c', '#c0392b', '#9b0000', '#f28a8a', '#00edff', '#1aa0bc', '#1cd8ff', '#ff92f4', '#d500a3', '#ffb300', '#d0cfec', '#ecf1f9', '#c8c8c8', '#656565', '#464646']

let wrapperRef;
class CreateHabitDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.colorPallet = null;
    this.state = {
      attachedFiles: [],
      isShowColorPallet: false,
      isShowTaskColorPallet: false,
      isShowFromSetTimePopup: false,
      isShowToSetTimePopup: false,
      isShowDontRepeat: false,
      isAllDay: false, 
      fromDate: new Date(),
      toDate: new Date(),
      fromTime: '',
      toTime: '',
      DoesNotRepeat: '', 
      isShowAssignToDrop: false,
      isShowTagsDrop: false,
      isSelectLinkToExist: false, 
      isRelatedDropOpen: false,
      assignToSelection: [],
      assignToChecked: '',
      selectedTags: [
        {id: 0, title: 'Risk', },
        {id: 2, title: 'Phase1', },
      ],
      isPriorityDropdown: false,
      priorities:[],
      selectedPriorId:0,
      selectedPeople: [],
      linkedTo:{},
      linkToExistSelection: [],


      selectedItems: [
        {id: 0, name: 'Ram'},
        {id: 1, name: 'Murthy'}
      ],
      buildQuitBtnId: 1,
      goalPeriodBtnId: 4,
      weekDaysBtnId: 3,
      selectedDate: '',
      isShareThroughEmail: false,
      habitTitleColor: '',
      setTimeBtnId: null,
      setTimeBtns: [],
      weekDaysBtns: weekDaysBtns,
      habitTitle : "",
      count:1,
      setTime:[],
      location:"",
      tags:[],
      tags1:[],
      users:[],
      users1:[],
      shareMail:"",
      saveTemplate:false,
      frequency:{},
      responsibleUsers:[],
      isEditCompDtlsOpen: false,
      company:[],
      anchorEl: null,
      selectedCompany:{},
      departments:[],
      selectedDepartment:{},
      teamsData:[],
      selectedTeam:{},
      logedUser:{},
      selectedProject:{},
      checkedTagsArray:[],
      tagsSelect:"",
      listOfRelatedTo:[],
      relatedSelect:"",
      taskFrequency:0,
      selectedQuick:{}
    }
  }
  componentDidMount(){
    // document.addEventListener('mousedown', this.handleClickOutside);
    this.fetchUsers()
    let requestBody1 = {
      query: `
        query getUserById($id:Int!) 
          {
            getUserById(id:$id){
              id
              username,
              emailIs,
              firstName,
              lastName,
              tenantId,
              departmentId,
              companyId,
            }
          }
          `,
          variables: {
            id:parseInt(localStorage.getItem('id')),
          }
    };

      axios({
        method: 'post',
        url: baseUrl.server,
        data: requestBody1,
        headers: {
            'Content-type': 'application/json'
        }
    }).then(res => {
      this.setState({logedUser:res.data.data.getUserById})
      this.fetchTeams(res.data.data.getUserById.companyId,res.data.data.getUserById.departmentId,res.data.data.getUserById.tenantId);
      this.fetchcompany(res.data.data.getUserById.companyId);
      this.fetchDepartment(res.data.data.getUserById.departmentId);
      this.fetchTags(res.data.data.getUserById.companyId);
        return res;
    }).catch(err => {
      console.log("Error in user==",err)
        return err;
    });
    this.fetchPriorities()
  }

  array=[];

  checkedTags=(id)=>{
    var tagString=""
    if(this.array.includes(id)){
      let index = this.array.indexOf(id)
      this.array.splice(index,1);
  }else{
    this.array.push(id)
  }
  this.array.map(uniq=>{
    if(this.array.length>1){
    tagString += " " + uniq.tagTitle
  }else{
    tagString = uniq.tagTitle
  }
  })
  this.setState({checkedTagsArray:this.array,tagsSelect:tagString})

  }



  arrayRelatedTo = [];
  checkBoxRelatedTo=(data)=>{
    var relatedSelect=""
    if(this.arrayRelatedTo.includes(data)){
      let index = this.arrayRelatedTo.indexOf(data)
      this.arrayRelatedTo.splice(index,1);
  }else{
    this.arrayRelatedTo.push(data)
  }
  this.arrayRelatedTo.map(uniq=>{
    if(this.arrayRelatedTo.length>1){
      relatedSelect += " " + uniq.title
  }else{
    relatedSelect = ""+uniq.title
  }
  })
  this.setState({listOfRelatedTo:this.arrayRelatedTo,relatedSelect:relatedSelect})
  }

  arrayToAssign = [];
  assignToCheck=(data)=>{
    
    if(this.arrayToAssign.includes(data)){
      let index = this.arrayToAssign.indexOf(data)
      this.arrayToAssign.splice(index,1);
  }else{
    this.arrayToAssign.push(data)
  }
  this.setState({assignToSelection:this.arrayToAssign})
  }

  checkBoxLinkTo=(data)=>{
    if(data!=this.state.linkedTo){
    this.setState({linkedTo:data,isSelectLinkToExist:false,isRelatedDropOpen:false,relatedSelect:""})
  }else{
    this.setState({linkedTo:data,isSelectLinkToExist:false,isRelatedDropOpen:false})
  }
    if(data.id==0){
      this.fetchTasks()
    }else if(data.id==1){this.fetchProject()}else if(data.id==2){this.fetchTicket()}else if(data.id==3){this.fetchObjective()}

  }



  submitHandler=()=>{
  }
  openEditCompDtlsOpen=(e)=>{
    e.stopPropagation()
     this.setState({ isEditCompDtlsOpen: true })
    }
  closeCompanyDrawer=()=>{
    this.setState({isEditCompDtlsOpen: false})
  }

  fetchPriorities = (companyId) => {
    const requestBody = {
        query: `
          query {
            priorities {
              id,
              priorityname,
            }
          }
        `
    };
  
    fetch(baseUrl.server, {
        method: 'POST',
        body: JSON.stringify(requestBody),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            if (res.status !== 200 && res.status !== 201) {
                throw new Error('Failed!');
            }
            return res.json();
        })
        .then(resData => {
            let priorities = resData.data.priorities;
            this.setState({ priorities: priorities });
        }) 
        .catch(err => {
            console.log("err==",err);
        });
  };

  fetchcompany = (companyId) => {
    const requestBody = {
        query: `
          query {
            getAllCompany {
              id,
              companyName,
              companyDescription
            }
          }
        `
    };
  
    fetch(baseUrl.server, {
        method: 'POST',
        body: JSON.stringify(requestBody),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            if (res.status !== 200 && res.status !== 201) {
                throw new Error('Failed!');
            }
            return res.json();
        })
        .then(resData => {
            let company = resData.data.getAllCompany;
            if(company!=null || company != undefined){
            company.map(cmp=>{
              if(companyId == cmp.id){
                this.setState({selectedCompany:cmp})
              }
            })
          }
            this.setState({ company: company });
        })
  
        .catch(err => {
            console.log(err);
        });
  };

  fetchUsers = (companyId) => {
    const getAllUsers = {
        query: `
          query {
            getAllUsers {
              id,
              username,
              emailIs
              firstName
              lastName
            }
          }
        `
    };
  
    fetch(baseUrl.server, {
        method: 'POST',
        body: JSON.stringify(getAllUsers),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            if (res.status !== 200 && res.status !== 201) {
                throw new Error('Failed!');
            }
            return res.json();
        })
        .then(resData => {
            let userAll = resData.data.getAllUsers;
            this.setState({ users:userAll });
        })  
        .catch(err => {
            console.log(err);
        });
  };


  fetchDepartment = (deptId) => {
    const requestBody1 = {
        query: `
          query {
            getAllDepartments {
              id,
              departmentName
            }
          }
        `
    };
  
    fetch(baseUrl.server, {
        method: 'POST',
        body: JSON.stringify(requestBody1),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            if (res.status !== 200 && res.status !== 201) {
                throw new Error('Failed!');
            }
            return res.json();
        })
        .then(resData => {           
            let departments = resData.data.getAllDepartments;
            if(departments !=null || departments != undefined){
            departments.map(depart=>{
              if(deptId == depart.id){
                this.setState({selectedDepartment:depart})
              }
            }) 
          }
            this.setState({ departments: departments });
        })
  
        .catch(err => {
            console.log(err);
        });
  };

fetchTags=(companyId)=>{
  let requestBody2 = {
    query: `
  query allTags($companyId:Int!) 
    {
      allTags(companyId:$companyId){
        id,
         tagTitle
        }
    }
`,
    variables: {
      companyId: companyId,
    }
  };

  axios({
    method: 'post',
    url: baseUrl.server,
    data: requestBody2,
    headers: {
      'Content-type': 'application/json'
    }
  }).then(res => {
    this.setState({ tags: res.data.data.allTags })
    return res
  }).catch(err => {
    console.log("ErrorTags==",err)
    return err;
  });
}

fetchTasks = () => {
  let allTaskData = {
    query: `
      query tasksList 
        {
            tasksList{
            id
            taskTitle,
          }
        }
        `,
  };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: allTaskData,
      headers: {
          'Content-type': 'application/json'
      }
  }).then(res => {
    let array = res.data.data.tasksList;
    linkRelatedRecords = [];
    this.setState({linkToExistSelection:res.data.data.tasksList})
    array.map(arr1=>{
      linkRelatedRecords.push({id:arr1.id,title:arr1.taskTitle})
    })
      return res
  }).catch(err => {
    console.log("Error in user==",err)
      return err;
  });
};

fetchProject = () => {
  let allProject = {
    query: `
      query getAllProjects 
        {
          getAllProjects{
            id
            projectName,
          }
        }
        `,
  };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: allProject,
      headers: {
          'Content-type': 'application/json'
      }
  }).then(res => {
    let array = res.data.data.getAllProjects;
    linkRelatedRecords = [];
    this.setState({linkToExistSelection:res.data.data.getAllProjects})
    array.map(arr1=>{
      linkRelatedRecords.push({id:arr1.id,title:arr1.projectName})
    })
      return res
  }).catch(err => {
    console.log("Error in user==",err)
      return err;
  });
};

fetchTicket = () => {
  let allTicket = {
    query: `
      query ticketsList 
        {
          ticketsList{
            id
            name,
          }
        }
        `,
  };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: allTicket,
      headers: {
          'Content-type': 'application/json'
      }
  }).then(res => {
    let array = res.data.data.ticketsList
    linkRelatedRecords = [];
    this.setState({linkToExistSelection:res.data.data.ticketsList})
    array.map(arr1=>{
      linkRelatedRecords.push({id:arr1.id,title:arr1.name})
    })
      return res
  }).catch(err => {
    console.log("Error in user==",err)
      return err;
  });
};


fetchObjective = () => {
  let allObjective = {
    query: `
      query getAllObjectives 
        {
          getAllObjectives{
            id
            objectiveTitle,
          }
        }
        `,
  };

    axios({
      method: 'post',
      url: baseUrl.server,
      data: allObjective,
      headers: {
          'Content-type': 'application/json'
      }
  }).then(res => {
    let array = res.data.data.getAllObjectives;
    linkRelatedRecords = [];
    this.setState({linkToExistSelection:res.data.data.getAllObjectives})
    array.map(arr1=>{
      linkRelatedRecords.push({id:arr1.id,title:arr1.objectiveTitle})
    })
      return res
  }).catch(err => {
    console.log("Error in user==",err)
      return err;
  });
};

  fetchTeams = (a,b,c) => {
    const requestBody = {
        query: `
          query getTeamByCompanyIdAndDepartmentId($companyId:Int,$departmentId:Int,$tenantId:Int)
             {
              getTeamByCompanyIdAndDepartmentId(companyId:$companyId,departmentId:$departmentId,tenantId:$tenantId){
              id,
              teamName
              }
            }
          `, variables:{
          companyId:a,
          departmentId : b,
          tenantId : c
        }
    };
  
    axios({
      method: 'post',
      url: baseUrl.server,
      data: requestBody,
      headers: {
          'Content-type': 'application/json'
      }
  }).then(resData => {
            let teamsData = resData.data.data.getTeamByCompanyIdAndDepartmentId;
            this.setState({ teamsData: teamsData, selectedTeam:teamsData[0] });
        })
        .catch(err => {
            console.log(err);
        });
  };

  handleClickOutside=(event)=> {
    if (wrapperRef && !wrapperRef.contains(event.target)) {
      this.setState({ 
        isShowColorPallet: false, 
        isShowFromSetTimePopup: false,
        isShowToSetTimePopup: false,
        isShowDontRepeat: false,
        isShowAssignToDrop: false,
        // isShowTagsDrop: false,
        isPriorityDropdown: false,
        isSelectLinkToExist: false,
        isRelatedDropOpen: false,
        isQuickCreateDropdown: false
      })
    }
  }  
  setWrapperRef=(node)=> wrapperRef = node; 
  button =(btnName, style, onClick)=> (
    <Button variant="contained" className={style} onClick={onClick}>
      {btnName}
    </Button>
  )

  removeOneTimerBtn=(id)=> {
    let setTimeBtns = this.state.setTimeBtns;
    setTimeBtns.splice(id, 1);
    this.setState({ setTimeBtns: setTimeBtns })
  }

  SelectMenuChangeHandler =event=> {
    this.setState({ [event.target.name]: event.target.value })}
  SelectMenuChangeResponsible = event=> {
    this.setState({ [event.target.name]: event.target.value})

}
  checkBoxHandler = event => this.setState({ [event.target.name]: event.target.checked });
  onChangeHandler =event=> this.setState({ [event.target.name]: event.target.value })
  checkBoxHandlerTemplate = event => this.setState({ saveTemplate: event.target.checked })
  closeDrawerHandler =()=> this.props.thisObj.setState({ isCTaskDrawerOpen: false });
  buildQuitBtnClickHandler =button=> this.setState({ buildQuitBtnId: button.btnId });
  goalPeriodBtnClickHandler =button=> this.setState({ goalPeriodBtnId: button.btnId });
  handleDateChange =date=> this.setState({ selectedDate: date });
  showColorPalletPopup=()=> this.setState({isShowColorPallet: !this.state.isShowColorPallet});
  showTaskColorPalletPopup=()=> this.setState({isShowTaskColorPallet: !this.state.isShowTaskColorPallet});
  showSetTimePopup=(id)=> this.setState({isShowSetTimePopup: !this.state.isShowSetTimePopup, setTimeBtnId: id});
  handleChangeComplete = (color, event) => {
    this.setState({ habitTitleColor: color.hex ,isShowTaskColorPallet:!this.state.isShowTaskColorPallet})
};
  weekDaysBtnClickHandler =button=> {
    let tempWeekDaysBtns = this.state.weekDaysBtns;
    tempWeekDaysBtns[button.btnId].isActive = !tempWeekDaysBtns[button.btnId].isActive
    this.setState({ weekDaysBtnId: button.btnId , frequency: tempWeekDaysBtns});
  }
  showPriorityDropdown=()=> this.setState({ isPriorityDropdown: !this.state.isPriorityDropdown,isShowTagsDrop:false })
  priorityHandleClick=(selectedObject) =>{
    this.setState({ selectedPriorId: selectedObject,isPriorityDropdown: !this.state.isPriorityDropdown});
  }
  peopleHandleClickfunction = (e, index,name) => {
    this.setState({[e.target.name] : index});
    if(this.state.selectedPeople.includes(name)){}else{
    this.state.selectedPeople.push(name)
    this.state.selectedItems.push(name)
    }
  }

submitFreq=(e)=>{
  this.setState({taskFrequency:e.target.value ,isShowDontRepeat: !this.state.isShowDontRepeat })
}
onChangeEstimateTime=(e)=>{
  this.setState({[e.target.name]:e.target.value})
}
  
  quickHandleClick=(selectedObject) =>{
    this.setState({selectedQuick: selectedObject,ticketTitle:`${selectedObject.ticketPrefix}-${selectedObject.title}`,
    ticketDescription:selectedObject.ticketDescription,isQuickCreateDropdown:!this.state.isQuickCreateDropdown,
    quickTemplate:selectedObject.title})
  }

  taskColorPalletPopup =()=> (
    <div className='color-pallet-popup' ref={this.setWrapperRef}>
      <span>Color palette</span>
        <TwitterPicker colors={colors} onClick={()=>this.setState({isShowTaskColorPallet:!this.state.isShowTaskColorPallet})} onChangeComplete={ this.handleChangeComplete }/>
    </div>
  )

  colorPalletPopup =()=> (
    <div className='create-task-tag-color-pallet-popup' ref={this.setWrapperRef}>
      <span>Color palette</span>
        <TwitterPicker colors={colors} onChangeComplete={ this.handleChangeComplete }/>
    </div>
  )
  dateChangeHandler=(date, dateType)=> {
    this.setState({ [dateType]: date })
  }
  setTimePopup =(time, timeType)=> (
    <div className='CT-set-time-popup' ref={this.setWrapperRef}>
      <Scrollbars className='time-scroll-container'>
        {time.map(timeToSet => <li onClick={()=> this.setState({ [timeType]: timeToSet, isShowFromSetTimePopup: false, isShowToSetTimePopup: false })}>{timeToSet}</li>)}
        </Scrollbars>
    </div>
  )

  handleChangeShare=(e)=>{
    this.setState({shareMail:e.target.value,isShareThroughEmail:true})
  }
  onChangeHabitTitle = (e)=>{
    this.setState({habitTitle:e.target.value})
  }
  removeAttachedFileHandler=(id)=> {
    let removeAttachedFile = this.state.attachedFiles;
    removeAttachedFile.splice(id, 1)
    this.setState({ attachedFiles: removeAttachedFile })
  }
  fileAttachmentHandler =event=> {
    let imageList = [];
    let extension = null;
    for(let i = 0; i < event.target.files.length; i++){
      extension = event.target.files[i].name.split('.')[1];
      if( imageFormats.includes(extension) ){
        this.imageToBase64Converter(event.target.files[i], result => {
          imageList = [...imageList, result]
          this.setState({
            attachedFiles: [...this.state.attachedFiles, { base64: result }]
          })
        })
      } else {
        this.setState({
          attachedFiles: [...this.state.attachedFiles, event.target.files[i]]
        })
      }
    }
  }
  imageToBase64Converter =(image, callback) =>{
    let reader = new FileReader();
    reader.readAsDataURL(image);
    reader.onload = ()=> {
      callback(reader.result)
    }
  }
  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  sideList=()=> (
    <div >

      {this.state.isEditCompDtlsOpen ? <EditCompanyDetails onClick={(e)=>e.stopPropagation()} isEditCompDtlsOpen={this.state.isEditCompDtlsOpen} allState={this.state} thisObj={this}/>:null}
      <div className='create-habit-layout'>
        <div className="create-habit-close-icon" onClick={this.closeDrawerHandler}>
          <img src={closeCreateHabitIcon} alt='close-icon' />
        </div>

        <div className='create-habit-main-section'>
          <div className="create-task-header-section">
            <div className="task-header-text">
              <p>Create Task</p>
              <p>Lorem ipsum is simply dummy text of the printing and <br></br>typesetting industry</p>
            </div>

            <div className='quick-create-using-template'>
              <div className='quick-create-template'>
                <div className='selected-item-container'>
                  <p>{this.state.quickTemplate ? this.state.quickTemplate : 'Quick create using template'}</p>
                </div>
                <div className="quick-drp-dwn-img">
                  <img src={DropDwnIcon} onClick={()=> this.setState({ isQuickCreateDropdown: !this.state.isQuickCreateDropdown })}/>
                </div>
              </div>
              <div className={`dropdown-menu-container ${this.state.isQuickCreateDropdown ? 'open-dropdown' : 'close-dropdown'}`} ref={this.setWrapperRef}>
                <div className="quick-create-search-bar d-flex align-items-center">                        
                    <InputBase
                        placeholder="Search of template"
                        className="search-input"
                        inputProps={{ 'aria-label': 'search' }}
                    />
                    <SearchIcon />
                </div>
                <Scrollbars className="custom-scroll" style={{height: 160 }}>
                {
                  quickCreateIcons.map(icon => 
                    <StyledMenuItem className="customized-ticket-list" onClick={()=> this.quickHandleClick(icon)} onClose={this.handleClose}>
                      <div className="quick-create-ticket-one d-flex">
                        {/* <img src={icon.icon} alt='icon'/> */}
                        <p>{icon.title}</p>
                      </div>
                      
                    </StyledMenuItem>
                )}
                </Scrollbars>
              </div>
            </div>
          </div>

          <div className='create-habit-title-container' >
            <>
              <TextField
                id="standard-basic"
                className={''}
                inputProps={{style:{color:`${this.state.habitTitleColor}`}}}
                value = {this.state.habitTitle}
                margin="normal"
                placeholder='Task title *'
                onChange={this.onChangeHabitTitle}
              />
              <div className='color-pallet-container'>
                <img src={colorPallet} alt='colorPallet' className='colorPallet' onClick={this.showTaskColorPalletPopup}/>
                {this.state.isShowTaskColorPallet ? this.taskColorPalletPopup() : null}              
              </div>
            </>
          </div>

          <div className='task-date-time-container'>
            <div className='task-date-time'>
              <div id='task-datepicket-btn'>
                  <Button variant="contained" className={'CT-custome-button'} onClick={'onClick'}>{ this.state.fromDate ? moment(this.state.fromDate).format("MMM, DD YYYY") :'From Date'}</Button>
                  <DatePicker showYearDropdown yearDropdownItemNumber={15} onChange={(date)=>this.dateChangeHandler(date, 'fromDate')} value={this.state.fromDate} clearIcon={null}/>
              </div>
              {
                !this.state.isAllDay ? 
                (
                  <div className='CT-time-setter'>
                    <Button variant="contained" className={'CT-custome-button CT-time-btn'} onClick={()=> this.setState({ isShowFromSetTimePopup: !this.state.isShowFromSetTimePopup })}>{this.state.fromTime ? this.state.fromTime : 'Start Time'}</Button>
                    { this.state.isShowFromSetTimePopup ? this.setTimePopup(time, 'fromTime') : null}
                  </div>
                ) : null
              }

            </div>
              <span id='to'>to</span>
            <div className='task-date-time'>
              <div id='task-datepicket-btn'>
                  <Button variant="contained" className={'CT-custome-button'} onClick={'onClick'}>{this.state.toDate ? moment(this.state.toDate).format("MMM, DD YYYY") :'To Date'}</Button>
                  <DatePicker onChange={(date)=>this.dateChangeHandler(date, 'toDate')} value={this.state.toDate} clearIcon={null}/>
              </div>
              {
                !this.state.isAllDay ? 
                (
                  <div className='CT-time-setter'>
                    <Button variant="contained" className={'CT-custome-button CT-time-btn'} onClick={()=> this.setState({ isShowToSetTimePopup: !this.state.isShowToSetTimePopup })}>{this.state.toTime ? this.state.toTime : 'Start Time'}</Button>
                    { this.state.isShowToSetTimePopup ? this.setTimePopup(time, 'toTime') : null}
                  </div>
                ) : null
              }

            </div> 

            <div className='CT-all-day'>
              <Checkbox name='isAllDay' className='CT-all-day-checkbox' checked={this.state.isAllDay} onChange={this.checkBoxHandler} value="checkedA" />
              <span>ALL DAY</span>
            </div>

            <div className='DNR-dropdown-container'>
              <div className='DNR-custome-non-library-dropdown'>
                <div className='DNR-selected-item-container'>
                  {this.state.taskFrequency=="0"?'Does not repeat' :this.state.taskFrequency=="1"?'Daily':
                  this.state.taskFrequency=="2"?'Weekly':this.state.taskFrequency=="3"?'Monthly': 'Custom'
                 }
                </div><img src={dropdownIcon} alt='dropdownIcon' onClick={()=> this.setState({ isShowDontRepeat: !this.state.isShowDontRepeat })}/>
              </div>

              <div className={`DNR-custome-dropdown-menu-container ${this.state.isShowDontRepeat ? 'DNR-custome-open-dropdown zIndex' : 'DNR-custome-close-dropdown'}`} ref={this.setWrapperRef} >
                  <ul onClick={(e)=> this.submitFreq(e)}>
                    <li value="0">Does not repeat</li>
                    <li value="1">Daily</li>
                    <li value="2">Weekly</li>
                    <li value="3">Monthly</li>
                    <li value="4">Custom</li>
                  </ul>
              </div>
            </div>            
          </div>  

          <div className='flex-container'>
            <div className='CT-assignTo-dropdown-container'>
                <div className='CT-assignTo-non-library-dropdown'>
                  <div className='CT-selected-item-container'>
                    {this.state.assignToSelection.length == 0 ? 'Assign To':  
                      this.state.assignToSelection.map( item=> 
                        <div className='CT-selected-item-template'>
                          <img id='avathar-img' src={profileIcon} alt='profileIcon'/>
                          <span>{item.firstName} {item.lastName}</span>
                          <img id='remove-img' src={decrementIcon} alt='decrementIcon' onClick={()=>this.assignToCheck(item)}/>
                        </div>
                    )}
                  </div><img className='ctm-drop-toggler' src={dropdownIcon} alt='dropdownIcon' onClick={()=> this.setState({ isShowAssignToDrop: !this.state.isShowAssignToDrop })}/>
                </div>

                <div className={`CT-assignTo-custome-dropdown-menu-container ${this.state.isShowAssignToDrop ? 'CT-assignTo-custome-open-dropdown zIndex' : 'CT-assignTo-custome-close-dropdown'}`} ref={this.setWrapperRef} >
                  <div className='assign-to-list-container'>
                    {this.state.users.map(assignTo => {
                      return (
                        <div className='assign-to-item'>
                          <div className='assign-item-avatar'> 
                            <img src={profileIcon} alt='profileIcon'/>
                            <span>{assignTo.firstName} {assignTo.lastName}</span>
                            <Checkbox className='avatar-item-checkbox' name='assignToChecked' onChange={()=>this.assignToCheck(assignTo)} checked={this.state.assignToSelection.includes(assignTo)} />
                          </div>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div> 
              <div className='CT-estimated-time'>
                <img id='estimatedTimeIcon' src={estimatedTimeIcon} alt='estimatedTimeIcon' />
                <TextField
                  className={''}
                  value={this.state.estimateTime}
                  name="estimateTime"
                  label="Estimated time (HH:MM)"
                  margin="normal"
                  variant="outlined"
                  onChange={this.onChangeEstimateTime}
                />  
              </div>
          </div>

          <div className='CT-text-area'>
            <TextareaAutosize className='notes-text-area' placeholder="Task description" />
          </div>

          
{/*****************************************Priotiy and Tags drop down****************************************** */}
          <div className='flex-container'>
            <div className={`CT-menu-container`} onClick={this.showPriorityDropdown}>
              <div className={`CT-priority-container ${this.state.isPriorityDropdown ? 'field-z-index' : null } `}>
                <div className='CT-selected-item-container'>
                  {this.state.selectedPriorId ?
                  <p  htmlFor="priority"   name="priority" value={this.state.selectedPriorId.id}>{this.state.selectedPriorId.priorityname}</p>
                  : <p  htmlFor="priority"   name="priority" value="">Priotiy</p>
                  }
                </div>
                <div className="CT-priority-drp-dwn-img">
                  <img src={DrpDwnIcn} onClick={this.showPriorityDropdown} alt='DrpDwnIcn'/>
                </div>
              </div>

              <div className={`CT-priority-dropdown-menu-container ${this.state.isPriorityDropdown ? 'dropdown-z-index CT-priority-open-dropdown' : 'dropdown-z-index close-dropdown'}`} ref={this.setWrapperRef}>
                { this.state.priorities != null || this.state.priorities != undefined?
                  this.state.priorities.map((icon, index) => 
                  <StyledMenuItem className="customized-ticket-source"  onClick={(e)=>{
                    e.stopPropagation();
                    this.priorityHandleClick(icon)}} onClose={this.handleClose}>
                    <div className="create-ticket-one-priority d-flex">
                      <div className="create-ticket-prio-img-text d-flex">
                        <img src={priorities[0].icon} alt='icon'></img>
                        <p value={icon}>{icon.priorityname}</p>
                      </div>
                        
                      <div className="create-ticket-src-radio">
                        <Radio
                          checked={this.state.selectedPriorId.id === icon.id}
                          value={icon.id}
                          name="radio-button-demo"
                          color="primary"
                          inputProps={{ 'aria-label': '' }}
                        />
                      </div>
                    </div>
                  </StyledMenuItem>
                ) : null
                }
              </div>
            </div>           
            <div className="CT-drawer-tags">
              <div className={`menu-container`}>
                    <div className={`tags-container ${this.state.isShowTagsDrop ? 'field-z-index' : null }`}>
                      <div className='selected-item-container'>
                      {this.state.tagsSelect !="" ? 
                          <p>{this.state.tagsSelect}</p>
                        : <p>Tags</p>}
                      </div>
                      <div className="tag-drp-dwn-img">
                        <img src={addNewIcon} onClick={()=> this.setState({ isShowTagsDrop:!this.state.isShowTagsDrop,isPriorityDropdown:false  })} alt='AddMultipleTag'/>
                      </div>
                    </div>

                    <div className={`tag-dropdown-menu-container ${this.state.isShowTagsDrop ? 'dropdown-z-index tag-open-dropdown' : 'dropdown-z-index close-dropdown'}`} >
                        <Scrollbars className="custom-scroll" style={{height: 115 }}>
                        {this.state.tags != null || this.state.tags != undefined ?
                          this.state.tags.map((icon, index) =>
                        <StyledMenuItem className="customized-ticket-source" onClose={this.handleClose}>
                          <div className="create-ticket-one-tag d-flex">
                            <div className="create-ticket-tag-img-text d-flex">
                              <div className="create-ticket-tag-check">
                                <Checkbox
                                  className="create-ticket-tags-chck"
                                  name="checkedTagsArray"
                                  value={this.state.checkedTagsArray}
                                  color="primary"
                                  onClick={()=>this.checkedTags(icon)}
                                />
                              </div>
                              <p>{icon.tagTitle}</p>
                            </div>
                            <img src={icon.tagTitle == "Risk" ? MediumIcon : (icon.tagTitle == "Completed") ? HighIcon : null }></img>
                          </div>

                        </StyledMenuItem>
                      ) : null
                      }
                        </Scrollbars>
                            <hr></hr>
                            <div className="create-ticket-search-tag d-flex justify-space-around">
                              <InputBase
                                  placeholder="Enter tag name"
                                  className="search-input"
                                  inputProps={{ 'aria-label': 'search' }}
                              />
                              <img src={SearchTagAdd} alt=""></img>
                            </div>
                            <div className="create-task-drawer-color-picker d-flex">
                              <img src={ClrPckrTray} alt="" onClick={this.showColorPalletPopup}></img>
                              {this.state.isShowColorPallet ? this.colorPalletPopup() : null}
                              <div className="task-circle-color-picker">
                                {/* <CirclePicker/> */}
                                <CirclePicker colors={color} circleSize={18} width="300px" circleSpacing={10} onChangeComplete={ this.handleChangeComplete }/>
                              </div>
                              
                            </div>
                    </div>
                </div>
            </div>
          </div>
    {/* <div className='flex-container'>
      <div className='tags-dropdown-container'>
          <div className='CT-tags-non-library-dropdown'>
            <img id='tags-image' src={tagsIcon} alt='tagsIcon'/>
            <div className='CT-selected-tags-container'>
            {this.state.DoesNotRepeat ? 'Assign To':  
                this.state.selectedTags.map( tag => 
                  <div className='CT-selected-item-template'  style={{ background: '#b4e0ff' }}>
                    <span>{tag.title}</span>
                    <img id='remove-img' src={decrementIcon} alt='decrementIcon' />
                  </div>
                )
            }
            </div>
            <img id='add-new-icon' src={addNewIcon} alt='dropdownIcon' onClick={()=> this.setState({ isShowTagsDrop: true })}/>
          </div>

          <div className={`tags-custome-dropdown-menu-container ${this.state.isShowTagsDrop ? 'tags-custome-open-dropdown zIndex' : 'tags-custome-close-dropdown'}`} ref={this.setWrapperRef} >
            <div className='tags-list-container'>
              {tagsData.map(tag => {
                return (
                  <div className='tags-item'>
                      <Checkbox className='tags-item-checkbox' name='assignToChecked' checked={this.state.assignToChecked} onChange={this.checkBoxHandler} value="checkedA" />
                      <span>{tag.title}</span> 
                      <span className='tags-color-holder'></span>
                  </div>
                )
              })}
            </div>
              <hr/>
              <div className='tag-name-adder'>
                <TextField
                  className={''}
                  margin="normal"
                  placeholder='Enter tag name '
                />
              </div>
          </div>
        </div> 
    </div> */}



{/*****************************************Priotiy and Tags drop down****************************************** */}




{/*****************************************Link to Section****************************************** */}
    <div className='flex-container'>
      <div className='link-to-exist-dropdown-container'>
        <div className={`LTE-non-library-dropdown ${this.state.isSelectLinkToExist ? 'field-z-index' : null }`}>
          <div className='LTE-selected-item-container'>
              <div className='link-to-selected-template'>
                <img src={selectLinkToIcon} alt='selectLinkToIcon'/>
                {this.state.linkedTo.title != null  ?<span>{this.state.linkedTo.title}</span> : <span>Select link to existing</span>}
              </div>
          </div><img className='LTE-drop-toggler' src={dropdownIcon} alt='dropdownIcon' onClick={()=> this.setState({ isSelectLinkToExist: !this.state.isSelectLinkToExist,isRelatedDropOpen:false })}/>
        </div>

        <div className={`LTE-custome-dropdown-menu-container ${this.state.isSelectLinkToExist ? 'dropdown-z-index LTE-custome-open-dropdown ' : 'LTE-custome-close-dropdown'}`} ref={this.setWrapperRef} >
          <div className='LTE-list-container'>
            {selectLinkToExisting.map(linkTo => {
              return (
                <div className='LTE-item'>
                  <div className='LTE-item-avatar'> 
                    <img className='link-to-icon' src={linkTo.icon} alt='profileIcon'/>
                    <span>{linkTo.title}</span> 
                    <Checkbox className='LTE-item-checkbox' value={linkTo} name='linkedTo' checked={this.state.linkedTo.id==linkTo.id ? true :false} onClick={()=>this.checkBoxLinkTo(linkTo)} />
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </div> 

      <div className='link-to-record-dropdown-container'>
        <div className={`LTR-non-library-dropdown ${this.state.isRelatedDropOpen ? 'field-z-index' : null }`}>
          <div className='LTR-selected-item-container'>
              <div className='link-to-related-selected-template'>
                <img src={linkToRelatedIcon} alt='selectLinkToIcon'/>
                {
                  this.state.relatedSelect!="" ?
                  <span>{`${this.state.relatedSelect}`} </span>
                  :<span>Link to <span>{this.state.linkedTo.id==0 ?'<Related Tasks>'
                  : this.state.linkedTo.id==1 ? '<Related Projects>' : this.state.linkedTo.id==2 ? '<Related Tickets>' : 
                  this.state.linkedTo.id==3 ? '<Related Objectives>' : '<Related Record>'}
                  </span> </span>
                }
              </div>
          </div><img className='LTR-drop-toggler' src={dropdownIcon} alt='dropdownIcon' onClick={()=> this.setState({ isRelatedDropOpen: !this.state.isRelatedDropOpen,isSelectLinkToExist: false })}/>
        </div>

        <div className={`LTR-custome-dropdown-menu-container ${this.state.isRelatedDropOpen ? 'dropdown-z-index LTR-custome-open-dropdown ' : 'LTR-custome-close-dropdown'}`} ref={this.setWrapperRef} >
          <div className='LTR-list-container'>
            {linkRelatedRecords.map(linkTo => {
              return (
                <div className='LTR-item'>
                  <div className='LTR-item-avatar'> 
                    {/* <img className='link-to-icon' src={linkTo.icon} alt='profileIcon'/> */}
                    <span>{linkTo.title}</span> 
                    <Checkbox className='LTR-item-checkbox' name='assignToChecked' onChange={()=>this.checkBoxRelatedTo(linkTo)} value={linkTo} />
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </div> 
    </div>

{/*****************************************Link to Section****************************************** */}

        </div>
        <div className='create-task-sub-section'>

        <div className="create-ticket-drawer-company-details">
          <div className="create-ticket-company-description d-flex justify-space-between">
            <div className="d-flex">
              <img src={CompanyLogo} alt='CompanyLogo' />
              {/* {this.state.company.map(company=>{
                if(this.state.logedUser.companyId == company.id){ */}
                            <div className="create-ticket-drawer-company-text">
                            
                                <p className="create-ticket-drawer-company-text-one">{this.state.selectedCompany.companyName}</p>
                                <p className="create-ticket-drawer-company-text-two">{this.state.selectedCompany.companyDescription}</p>
                            </div>
              {/* }} )} */}
            </div>
            <div className="create-ticket-drawer-company-details-edit">
                <img src={CompanyEditIcon} onClick={(e)=>this.openEditCompDtlsOpen(e)} alt='CompanyEditIcon'/>
            </div>
          </div>
          <div className="department-and-team-name">

            {/* { this.state.departments != null || this.state.departments != undefined ? */}
              {/* this.state.departments.map(dept=>{ */}
                {/* if(this.state.logedUser.departmentId == dept.id) */}
                     { this.state.selectedDepartment ?  <p>{this.state.selectedDepartment.departmentName}</p> : <p>Department Name</p>
                    }
              {/* }) */}
              {/* : null */}
            {/* } */}
            {
              this.state.selectedTeam.teamName ? <p className="team-name">{this.state.selectedTeam.teamName}</p> : <p className="team-name">Team Name</p>
            }
            
          </div>
        </div>

          

              <div className='share-through-email-section'>
                <div>
                  <span>Share</span>       
                  <Checkbox className='share-through-email-checkbox' checked={this.state.isShareThroughEmail} onChange={()=>this.setState({isShareThroughEmail:!this.state.isShareThroughEmail})} value="" />
                </div>

                <FormControl disabled={this.state.isShareThroughEmail?false:true} variant="outlined" className='create-habit-customised-dropdown-menu'>
                  <InputLabel htmlFor="name-disabled">Enter email</InputLabel>
                  <div className=''>
                    <img src={emailIcon} alt='emailIcon'/>
                    <Select value={this.state.shareMail} onChange={this.handleChangeShare} inputProps={{}}>
                      {this.state.users!=null || this.state.users!=undefined ?
                        this.state.users.map(user=>{
                        return <MenuItem value={user.emailIs}>{user.firstName} {user.lastName}</MenuItem>
                        }): null
                      }
                    </Select>
                  </div>
                </FormControl>               
              </div> 

              <div className='create-task-attachment'>
            <span>Attachments</span>
            <div className={`CT-attached-files-container ${!this.state.attachedFiles ? 'align-center' : 'space-equaly '}`}>
              {this.state.attachedFiles!=null || this.state.attachedFiles!=undefined ?
                this.state.attachedFiles.map((file, index) => {
                return (
                  <div className='attached-file-template'>
                    <img className='attached-file-remove' src={removeAttachedFile} alt=''onClick={()=> this.removeAttachedFileHandler(index)}/>
                    {
                      file.base64 ? 
                      <img src={file.base64} alt='file'/> 
                      : 
                      file ? 
                        <img src={ fileIcons.map(icon=> icon[file.name.split('.')[1]]) } alt=''/>
                      : null
                    }
                    
                  </div>
                )
              }): null
              }
              <div className='CT-attachment-button'>
                <Button className='file-attach-button'>
                  <input
                      type="file"
                      className="custom-file-input"
                      id="inputGroupFile01"
                      multiple={true}
                      onChange={this.fileAttachmentHandler}
                  />
                </Button>
                <p>Drag attach file, <br/>or <span>browse</span></p>
              </div>                          
            </div>
          </div>
        </div>
      </div>

      <div className='create-habit-footer-section'>
          <div>
            <Checkbox className='Save-as-an-Idea-checkbox' checked={this.state.saveTemplate} onChange={this.checkBoxHandlerTemplate} value={this.state.saveTemplate} />
            <span>Save as an Idea for future use</span>
          </div>

          <Button variant="contained" className="ch-submit-button" onClick={this.submitHandler}>
                Submit
          </Button>
        </div>      
    </div>
  )
  
  render() {
    return (
      <Drawer onClick={this.closeCompanyDrawer} anchor="right" open={this.props.isCTaskDrawerOpen} className='create-ticket-drawer'>
        {this.sideList('right')}
      </Drawer>
    )
  }
}

export default CreateHabitDrawer;
